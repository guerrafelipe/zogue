# -*- encoding: utf-8 -*-
import mysql.connector
from clint.textui import colored
import os, sys
import nltk
from nltk.tokenize import PunktSentenceTokenizer, word_tokenize
from nltk.corpus import machado
from nltk.tag import StanfordNERTagger
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import style
from nltk import pos_tag
from nltk.chunk import conlltags2tree
from nltk.tree import Tree
import wikipedia
from nltk.corpus import stopwords
from bs4 import BeautifulSoup
from urllib2 import urlopen
import urllib

#MUDA O ENCODING
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')

"""
Das noticias ja validades pelos 'usuarios vip' extrair algumas caracteristicas, como
nome de pessoas 
"""

# Tag tokens with standard NLP BIO tags
def bio_tagger(ne_tagged):
		bio_tagged = []
		prev_tag = "O"
		for token, tag in ne_tagged:
			if tag == "O": #O
				bio_tagged.append((token, tag))
				prev_tag = tag
				continue
			if tag != "O" and prev_tag == "O": # Begin NE
				bio_tagged.append((token, "B-"+tag))
				prev_tag = tag
			elif prev_tag != "O" and prev_tag == tag: # Inside NE
				bio_tagged.append((token, "I-"+tag))
				prev_tag = tag
			elif prev_tag != "O" and prev_tag != tag: # Adjacent NE
				bio_tagged.append((token, "B-"+tag))
				prev_tag = tag
		return bio_tagged

# Create tree       
def stanford_tree(bio_tagged):
	tokens, ne_tags = zip(*bio_tagged)
	pos_tags = [pos for token, pos in pos_tag(tokens)]

	conlltags = [(token, pos, ne) for token, pos, ne in zip(tokens, pos_tags, ne_tags)]
	ne_tree = conlltags2tree(conlltags)
	return ne_tree


# Parse named entities from tree
def structure_ne(ne_tree):
	ne = []
	for subtree in ne_tree:
		if type(subtree) == Tree: # If subtree is a noun chunk, i.e. NE != "O"
			ne_label = subtree.label()
			ne_string = " ".join([token for token, pos in subtree.leaves()])
			ne.append((ne_string, ne_label))
	return ne

def get_entities(title):
	train_corpus = machado.raw('romance/marm05.txt')
	st = StanfordNERTagger('../../stanford-ner/classifiers/english.all.3class.distsim.crf.ser.gz',
						   '../../stanford-ner/stanford-ner.jar',
						   encoding='utf-8')
	tokenized_text = word_tokenize(title)
	classified_text = st.tag(tokenized_text)
	#List of tuples [('A', 'PERSON'), ('B', 'ORGANIZATION')]
	entities_list = structure_ne(stanford_tree(bio_tagger( classified_text )))
	pt_stopwords = stopwords.words('portuguese')
	result_dic = {}
	result_dic["people"] = []
	result_dic["organizations"] = []
	for t in entities_list:
		try:
			index_t = entities_list.index(t)
			if t[1] == "PERSON":
				name = t[0]
				name_splited = name.split()
				for n in name_splited:
					n = n.lower()
				f_name = name_splited[0]
				for h in name_splited:
					if h in pt_stopwords:
						entities_list.remove(index_t)
						raise GetOutOfLoop
					else:
						continue
				result_dic["people"].append(name)
	
			elif t[1] == "ORGANIZATION":
				name = t[0]
				name_splited = name.split()
				for n in name_splited:
					n = n.lower()
				for h in name_splited:
					if h in pt_stopwords:
						entities_list.remove(index_t)
						raise GetOutOfLoop
					else:
						continue
				result_dic["organizations"].append(name)
			else:
				continue
		
		except GetOutOfLoop:
			continue

	#{ people : [], locations : [], organizations : [] }
	return result_dic

def searchImage():
	#/var/www/html/zogue/app/script/python
	os.chdir("../../public/xml/pages")

	cnx = mysql.connector.connect(user='root', database='zogue_site_development')
	cursor = cnx.cursor( dictionary = True, buffered=True )
	cursor.execute("SELECT Title, suggested_category FROM human_interactions WHERE DATE(created_at) = CURDATE();")	   
	data = cursor.fetchall()

	#{name : related_category}
	person_dic = {}
	train_corpus = machado.raw('romance/marm05.txt')
	st = StanfordNERTagger('../../stanford-ner/classifiers/english.all.3class.distsim.crf.ser.gz',
						   '../../stanford-ner/stanford-ner.jar',
						   encoding='utf-8')
	#print("Apos stanford:" + colored.blue(os.path.dirname(os.path.abspath(__file__))))
	person_dic = {}
	organization_dic = {}
	for d in data:
		print("________________________________________________________")
		print("Titulo:" + colored.green(d["Title"]))
		print("Categoria:" + colored.blue(d["suggested_category"]))
		title = d["Title"]
		category = d["suggested_category"]
		tokenized_text = word_tokenize(title)
		classified_text = st.tag(tokenized_text)
		#List of tuples [('A', 'PERSON'), ('B', 'ORGANIZATION')]
		entities_list = structure_ne(stanford_tree(bio_tagger( classified_text )))
		pt_stopwords = stopwords.words('portuguese')

		for t in entities_list:
			if t[1] == "PERSON":
				name = t[0]
				name_splited = name.split()
				
				for n in name_splited:
					n = n.lower()

				f_name = name_splited[0]
				for p in pt_stopwords:
					if p not in name_splited:
						person_dic[name] = category
						#print(" ->Pessoa detectada:" + colored.blue(name))
					else:
						continue
				
			elif t[1] == "ORGANIZATION":
				name = t[0]
				name_splited = name.split()

				for n in name_splited:
					n = n.lower()

				for o in pt_stopwords:
					if o not in name_splited:
						organization_dic[ t[0] ] = category
						#print(" ->Organizacao detectada:" + colored.blue( t[0] ))
					else:
						continue
			else:
				continue
		print("_______________________________________________________")           
	cursor.close()
	cnx.close()

	print(colored.red(person_dic))
	print(colored.blue(organization_dic))	 
		
	if os.path.isdir("../../pictures"):
		os.chdir("../../pictures")
	else:
		os.mkdir("../../pictures")
		os.chdir("../../pictures")        

	print("Linha 144:" + os.path.dirname(os.path.abspath(__file__)))
	wikipedia.set_lang("pt")
	for key, value in person_dic.iteritems():		
		category = value
		if os.path.isdir(category): 
			os.chdir(category)
		else:
			os.mkdir(category)
			os.chdir(category)

		try:
			print("Linha 158:" + os.path.dirname(os.path.abspath(__file__)))
			entity_name = str(key)
			page = wikipedia.page( entity_name )
			print("_______________________________________")
			print("Palavra:" + colored.blue(key))
			print("Titulo da pagina Wikipedia:" + colored.blue(page.title))
			print("Categoria:" + colored.blue(value) )
			
			images_list = page.images
			for img in images_list:
				"""
				 Usar regular expression para ver se contem o nome da pessoa
				 AS IMAGENS DE INTERESSE CONTEM O NOME DA ENTIDADE E UM FORMATO DIFERENTE DE SVG
				 https://upload.wikimedia.org/wikipedia/commons/3/32/Paul_and_Linda_McCartney.jpg
				 https://upload.wikimedia.org/wikipedia/commons/c/c4/The_Beatles_in_America.JPG'
				 https://upload.wikimedia.org/wikipedia/commons/c/c4/The_Beatles_in_America.JPG'
				 https://upload.wikimedia.org/wikipedia/commons/4/44/Foto_de_%C3%A9poca_de_Michel_Temer_disponibilizado_pela_assessoria_%285%29.jpg
				"""
				normalized_entName = entity_name.replace(" ", "_")
				normalized_entName = normalized_entName.lower()

				file_name = img.split("/")[-1]
				ffile = file_name
				file_name = file_name.lower()
				file_type = file_name[-3:]
				if normalized_entName in file_name and file_type != "svg":
					#print(os.path.dirname(os.path.abspath(__file__)))
					extension = ffile.split(".")[-1]
					resource = urllib.urlopen(img)
					output = open(normalized_entName + "." + extension,"wb")
					output.write(resource.read())
					output.close()

					print("____________________________________")
					print("Imagem baixada")
					print("Nome do arquivo:" + colored.blue(file_name))
					print("Url da imagem:" + colored.blue(img))
					print("_____________________________________")		
			print("Url:" + colored.blue(page.url))
			print("________________________________________")
			
		except wikipedia.exceptions.DisambiguationError:
			print("PALAVRA AMBIGUA" + colored.red(key))
			continue
			os.chdir("..")

		except wikipedia.exceptions.PageError:
			print("PALAVRA ERRADA:" + colored.red(key))
			continue
			os.chdir("..")

		#os.chdir("../../script/python")
		os.chdir("..")

	"""
		NLTK'S NER -> Less accurate than Stanford's NER but less computationaly expensive
		custom_sent_tokenizer = PunktSentenceTokenizer( train_corpus[0:500] )
		tokenized = custom_sent_tokenizer.tokenize( title )
		for i in tokenized:
			words = nltk.word_tokenize(i)
			tagged = nltk.pos_tag(words)
			#return a nested nltk.tree.Tree
	        namedEnt = nltk.ne_chunk(tagged, binary=True)
	        #namedEnt.draw()
	        print(namedEnt)
	        #named_entities = []
	        #for t in namedEnt.subtrees():
	        #    if t.label() == 'NE':
	        #        named_entities.append(t)
	        #print(named_entities)
	"""

if __name__ == "__main__":
	searchImage()