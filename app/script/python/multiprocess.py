from multiprocessing import Pool
from time import sleep

def start_function_for_processes(n):
	sleep(.5)
	result_sent_back_to_parent = n * n
	print(result_sent_back_to_parent)

if __name__ == "__main__":
	#Create a pool with 5 processes
	p = Pool(5)
	p.map(start_function_for_processes, range(100))