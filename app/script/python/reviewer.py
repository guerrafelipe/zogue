# -*- encoding: utf-8 -*-
import os, sys
import pymysql
import pymysql.cursors
import settings
import miner
from clint.textui import colored

#MUDA O ENCODING
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')

gambiara_dic = {'ciencia':1, 'economia':2, 'entretenimento':3, 'esportes':5, 'politica':4, 'tecnologia':6}    
def returnSubcategory(subcategory_id):
    subcategories = {}
    cnx = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database = settings.database, charset = settings.charset)
    cursor = cnx.cursor(pymysql.cursors.DictCursor)   
    cursor.execute("SELECT Name FROM subcategories WHERE id = '%s';" % str(subcategory_id))     
    data = cursor.fetchone()
    if data is not None:
        name = data["Name"]
    cursor.close()
    cnx.close()
    return name
    
def review():	
	cnx = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database = settings.database, charset = settings.charset)
	cursor = cnx.cursor(pymysql.cursors.DictCursor)
	cursor.execute("SELECT Title, new_id, suggested_category FROM human_interactions WHERE DATE(created_at) = CURDATE() AND review = 1;")     
	data = cursor.fetchall()    
	#{new_id, subcategory_id}
	change_dic = {}
	for d in data:
		title = d["Title"]
		new_id = d["new_id"]
		suggested_category = d["suggested_category"]
		subcategory_id = miner.discover_subcategory(title, gambiara_dic[suggested_category])
		print("->Chamada: " + colored.blue(title))
		print("  ID da noticia: " + colored.blue(str(new_id)))
		print("  Categoria Sugerida: " + colored.red(suggested_category))

		if subcategory_id == 0:
			print(colored.red("  Nenhuma subcategoria identificada"))
			change_dic[new_id] = 0
		else:
			subcategory_name = returnSubcategory(subcategory_id)
			print("  Subcategoria: " + colored.red(subcategory_name))
			change_dic[new_id] = subcategory_id
	cursor.close()
	cnx.close()

	cnx = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database = settings.database, charset = settings.charset)
	cursor = cnx.cursor(pymysql.cursors.DictCursor)
	for c in change_dic:
		cursor.execute("UPDATE news SET subcategory_id=%s  WHERE id=%s;" %(change_dic[c], c))     
		cnx.commit()
	cursor.close()
	cnx.close()

	cnx = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database = settings.database, charset = settings.charset)
	cursor = cnx.cursor(pymysql.cursors.DictCursor)
	for c in change_dic:
		cursor.execute("UPDATE human_interactions SET review=0 WHERE new_id=%s;" %c)
		cnx.commit()
	cursor.close()
	cnx.close()
	print(colored.green("Tarefa realizada com sucesso"))

if __name__ == "__main__":
	settings.init("development")
	review()
