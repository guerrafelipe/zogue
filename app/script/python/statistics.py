# -*- encoding: utf-8 -*-
from __future__ import division
import os, sys
import glob
import time
from datetime import datetime
from clint.textui import colored
import xml.dom.minidom
import pymysql
import pymysql.cursors
import matplotlib.pyplot as plt
import numpy as np
import settings

#MUDA O ENCODING
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')

def getSites():
    #Number of sites in sites.xml
    urls = []
    DOMTree = xml.dom.minidom.parse("../../public/xml/sites.xml")
    sites = DOMTree.documentElement
    site = sites.getElementsByTagName("site")
    for s in site:
        u = s.getElementsByTagName("url")[0]
        if u:
            urls.append(u.childNodes[0].data)
        else:
            continue
    return len(urls)

def getSitesByCategory():
    #Parse do arquivo ../../public/xml/sites_by_categorie.xml
    DOMTree2 = xml.dom.minidom.parse("../../public/xml/sites_by_categorie.xml")
    sites2 = DOMTree2.documentElement
    categorie = sites2.getElementsByTagName("categoria")
    urls2 = []
    for c in categorie:
        site2 = c.getElementsByTagName("site")
        for s in site2:
            u2 = s.getElementsByTagName("url")[0]
            if u2:
                urls2.append(u2.childNodes[0].data)
            else:
                continue

    return len(urls2)

def drawEvolution():
    #For primary_classification
    cnx2 = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database = settings.database, charset = settings.charset)
    cursor2 = cnx2.cursor(pymysql.cursors.DictCursor)
    cursor2.execute("SELECT primary_classification, DATE(created_at) FROM statistics;")
    data = cursor2.fetchone()
    date = data["DATE(created_at)"]
    acc = data["primary_classification"]
    strDate = date.strftime('%d/%m')
    print(strDate)
    cursor2.close()
    cnx2.close()

    x = np.array([ 0,1,2,3 ])
    y = np.array([ 20,21,22,23 ])

    my_xticks = [ strDate ]
    plt.xticks(x, my_xticks)
    plt.plot(x, y)
    plt.show()


def performance(count = 1, onlyToday = True, execution_index = "1", nobreak = True):
   
    """
    ======================================================================
    Eficiencia do PROCESSO 0 (genesis.py)
    -> eficiencia = numero_de_paginas_baixadas / numero_esperado_de_paginas
    -> eh se esperado que o valor da eficiencia gire em torno de 90% ~ 100%
    =======================================================================
    """

    #Numero esperado eh o numero de sites que eu esperava baixar
    count += 1
    numero_esperado = 0
    numero_esperado += getSites()
    numero_esperado += getSitesByCategory()
    numero_esperado *= count

    blacklist_genesis = []
    os.chdir("../../public/pages")
    folders = glob.glob("*")
    n_sites = getSites()
    n_sites_by_category = getSitesByCategory()
    sumN = n_sites + n_sites_by_category

    numero_real = 0
    for f in folders:
        os.chdir(f)
        files_list = glob.glob("*" + time.strftime("%d:%m:%Y") + "*.html")
        # o erro mora aqui:
        if count == len(files_list):
            numero_real += len(files_list)
        else:
            blacklist_genesis.append(f)
        os.chdir("..")

    #EFICIENCIA DO PRIMEIRO PROCESSO: numero_real / numero_esperado
    eficiencia = numero_real / numero_esperado
    eficiencia *= 100
    print("______________________________________________________________")
    print("Eficiencia do primeiro processo: " + colored.green(str(eficiencia)+"%"))
    print("Numero de sites baixados no total:" + colored.blue(numero_real))
    print("Numero de sites no 'Test Set': " + colored.blue(str(getSites())) )
    print("Numero de sites no 'Train Set': " + colored.blue(str(getSitesByCategory())))

    if blacklist_genesis:
        print("BLACKLIST:")
        for b in blacklist_genesis:
            #Salvar numa tabela do banco de dados para penalizar o site no ranking geral
            print " ->" + colored.red(b)
    print("______________________________________________________________")

    """
    =========================================================================
    Eficiencia do Processo 1 (injection.py + runner.py + mover.py)
    -> eficiencia = numero_de_arquivos_xml / numero_esperado_de_arquivos_xml
    -> Um erro aqui pode indicar uma falha em injection.py, runner.py ou mover.py
    =========================================================================
    """
    blacklist_runner = []
    os.chdir("../xml/pages")
    folders2 = glob.glob("*")
    numero_real2 = 0
    for f in folders2:
        os.chdir(f)
        files_list2 = glob.glob("*" + time.strftime("%d:%m:%Y") + "*.xml")

        if count == len(files_list2):
            numero_real2 += len(files_list2)
        else:
            blacklist_runner.append(f)
        os.chdir("..")

    eficiencia2 = numero_real2 / numero_esperado
    eficiencia2 *= 100

    #Verificar se nao sobra nenhum XML na pasta script/php
    print("______________________________________________________________")
    print("Eficiencia do segundo processo: " + colored.green(str(eficiencia2)+"%"))

    #print(colored.blue(os.path.dirname(os.path.abspath(__file__))))
    if len(folders2) == sumN:
        print("Numero de pastas em ../xml/pages: " + colored.green(str(len(folders2))))
    else:
        print("Numero de pastas em ../xml/pages: " + colored.red(str(len(folders2))))
        #print("OBS: Algo esta errado aqui. O numero de pastas deveria ser: " + str(getSites() + getSitesByCategory()) )
    if blacklist_runner:
        print("BLACKLIST:")
        for b in blacklist_runner:
            print " ->" + colored.red(b)
    print("______________________________________________________________")

    """
    ===========================================================================
    Eficiencia do Processo 2 (isaac.py)
    ->Toma como base a classificao feita no website e registrada na tabela
    human_interactions
    -> eficiencia = numero_de_classificacoes_certas / numero_total_de_classificacoes
    ->Eh mais facil medir as classificacoes erradas e subtrair das certas:
    classificacoes erradas + classificacoes certas = total
    classificacoes certas = total - erradas
    ->
    ===========================================================================
    """

    #Usa o metodo get_list de builder.py
    #print()
    os.chdir("../../../script/python")

    cnx = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database = settings.database, charset = settings.charset)
    cursor = cnx.cursor(pymysql.cursors.DictCursor)
    cursor.execute("SELECT Title, category_id FROM news WHERE DATE(created_at) = CURDATE() AND execution_index = '%s';" % execution_index)
    data = cursor.fetchall()
    news_dic = {}
    gambiara_dic = {1 : 'ciencia', 2 : 'economia', 3 : 'entretenimento', 5 : 'esportes', 4 : 'politica', 6 : 'tecnologia'}
    for d in data:
        if d is not None:
             title = d["Title"]
             category_id = d["category_id"]
             category = gambiara_dic[category_id]

             #{categoria : chamada}
             news_dic[category] = title
    cursor.close()
    cnx.close()

    #print(colored.yellow(news_dic))
    if len(news_dic) > 0:
        news_number = len(news_dic)
        if nobreak == False:
            #A soma tem que ser igual ao numero total de noticias do dia
            numero_acertos = 0
            numero_erros = 0
            #A SOMA TEM QUE SER IGUAL len(value)
            cnx = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database = settings.database, charset = settings.charset)
            cursor = cnx.cursor(pymysql.cursors.DictCursor)
            print("________________________________________________________")
            count = 0
            for key, value in news_dic.iteritems():
                        cursor.execute("SELECT suggested_category FROM human_interactions WHERE Title = '%s';" %str(value))
                        data = cursor.fetchone()
                        if data is not None:
                            category = data["suggested_category"]
                            print("Noticia:" + colored.blue(value))
                            print("Categoria escolhida automaticamente:" + colored.blue(key))
                            print("Categoria sugerida pelos usuarios:" + colored.blue(category))

                            if key == category:
                                print(colored.green("CLASSIFICAO 'AUTOMATICA' CORRETA"))
                                numero_acertos += 1

                            else:
                                print(colored.red("CLASSIFICAO 'AUTOMATICA' ERRADA"))
                                numero_erros += 1
                            count += 1
            cursor.close()
            cnx.close()
            print("________________________________________________________")
            eficiencia_3 = 0.0
            eficiencia3 = numero_acertos / count
            eficiencia3 *= 100
            print("Numero de acertos:" + colored.blue(str(numero_acertos)))
            print("Numero de erros:" + colored.blue(str(numero_erros)))
            print("Eficiencia:" + colored.blue(str(eficiencia3) + "%"))
            print("________________________________________________________")
            print("__________________________________________________________")
        
            eficiencia_global = 0.0
            eficiencia_global = (eficiencia + eficiencia2 + eficiencia3) / 3
            print("A eficiencia global do script vale:" + colored.blue(str(eficiencia_global) + "%"))
            print("__________________________________________________________")
            
            print("__________________________________________________________")
            comment = raw_input(colored.red("Digite um comentario:"))
            print("__________________________________________________________")

            cnx = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database = settings.database, charset = settings.charset)
            cursor = cnx.cursor()
            add_statistic = ("INSERT INTO statistics (listed_sites, genesis, runner, primary_classification, news_number, rightansw_number, global , created_at, updated_at, comment) VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s)")
            new_sta = (numero_esperado, eficiencia, eficiencia2, eficiencia3, news_number, numero_acertos, eficiencia_global, datetime.now(), datetime.now(), comment)
            cursor.execute(add_statistic, new_sta)
            cnx.commit()
            cursor.close()
            cnx.close()
        else:
            eficiencia_global = 0.0
            eficiencia_global = (eficiencia + eficiencia2) / 2
            print("A eficiencia global do script vale:" + colored.blue(str(eficiencia_global) + "%"))
            comment = "Modo nobreak, execucao:" + str(execution_index)
            cnx = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database = settings.database, charset = settings.charset)
            cursor = cnx.cursor()
            add_statistic = ("INSERT INTO statistics (listed_sites, genesis, runner, primary_classification, news_number, rightansw_number, global , created_at, updated_at, comment) VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s)")
            new_sta = (0, eficiencia, eficiencia2, 0, news_number, 0, eficiencia_global, datetime.now(), datetime.now(), comment)
            cursor.execute(add_statistic, new_sta)
            cnx.commit()
            cursor.close()
            cnx.close()

    else:
        print(colored.red("Nao ha noticias para adicionar!"))
        cnx = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database= settings.database, charset = settings.charset)
        cursor = cnx.cursor()
        add_statistic = ("INSERT INTO statistics (listed_sites, genesis, runner, primary_classification, news_number, rightansw_number, global , created_at, updated_at, comment) VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s)")
        new_sta = (0, 0, 0, 0, 0, 0, 0, datetime.now(), datetime.now(), "Nenhuma noticia adicionada")
        cursor.execute(add_statistic, new_sta)
        cnx.commit()
        cursor.close()
        cnx.close()
        print("Estatistica adicionada")

if __name__ == "__main__":
    performance()
    #drawEvolution()