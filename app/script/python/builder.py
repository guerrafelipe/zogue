# -*- encoding: utf-8 -*-
import requests
import os, sys
import glob
import shutil
import sched, time
from datetime import datetime
from clint.textui import colored
import xml.dom.minidom
from xml.dom.minidom import parse, parseString
from bs4 import BeautifulSoup
import ai.isaac
import ai.multiclass_classifier
import io
from time import gmtime, strftime
import json
import pymysql
import pymysql.cursors
import shutil
from random import randint
from nltk.tokenize import word_tokenize
import settings
import miner
#MUDA O ENCODING
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')
#Pega os arquivos XML na pasta ../../public/xml/pages/[NOME] e faz as analises
#In python, empty strings are considered 'falsy'

def returnTest(onlyToday = True, execution_index = "1"):
    os.chdir("../../public/xml/pages")    
    folders = glob.glob("*")
    
    #Retornar dinamicamente com o MySql
    blacklist = ["economia", "esportes", "politica", "ciencia", "tecnologia", "entretenimento"]
    folders2 = []    
    for f in folders:    
        categorie = f.split("_")[0]
        if categorie not in blacklist:
          folders2.append(f)
        else:
          continue
    big_list = {}    
    for f in folders2:
        os.chdir(f)       
        if onlyToday == True:
          files = glob.glob("*" + time.strftime("%d:%m:%Y") + "(" + execution_index + ")" +".xml")
        else:
          files = glob.glob("*.xml")  
        
        if len(files) > 0:        
            for g in files:
                    file1 = io.open(g, encoding="utf8")
                    content = file1.read()
                    file1.close()
                    if not content:
                        continue
                    
                    DOMTree = xml.dom.minidom.parseString(content)
                    noticiario = DOMTree.documentElement
                    noticias = noticiario.getElementsByTagName("noticia")
                    
                    for s in noticias:
                        count_n = len(s.getElementsByTagName("chamada"))
                        count_u = len(s.getElementsByTagName("link"))
                       
                        if count_n == count_u:
                            n = s.getElementsByTagName("chamada")[0]
                            u = s.getElementsByTagName("link")[0]
                            try: 
                                chamada_texto = n.childNodes[0].data
                                chamada_texto = chamada_texto.strip()
                                
                                chamada_link =  u.childNodes[0].data
                                chamada_link = chamada_link.strip()
                                
                                if chamada_texto and chamada_link:
                                    chamada_texto = chamada_texto.replace("\n","")                                    
                                    chamada_texto = chamada_texto.replace("\t","")
                                    #Evita erros
                                    chamada_texto = chamada_texto.replace("'",'&quot;')
                                    chamada_texto = chamada_texto.replace('"','&quot;')

                                    spl = chamada_texto.split()
                                                                       
                                    if len(spl) > 3:
                                        chamada_texto = str(chamada_texto)
                                        chamada_link = str(chamada_link)
                                        chamada_texto = chamada_texto.encode("utf8")
                                        chamada_link = chamada_link.encode("utf8")                                        
                                        
                                        #Verifica se o link nao ta quebrado                                        
                                        response = requests.get(chamada_link)
                                        html = response.text
                                        
                                        big_list[chamada_texto] = chamada_link
                            except:
                                continue
        os.chdir("..")
    #Retorna um dicionario {chamada : link}
    return big_list  

def returnSubcategory(subcategory_id):
    subcategories = {}
    cnx = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database = settings.database, charset = settings.charset)
    cursor = cnx.cursor(pymysql.cursors.DictCursor)   
    cursor.execute("SELECT Name FROM subcategories WHERE id = '%s';" % str(subcategory_id))     
    data = cursor.fetchone()
    if data is not None:
        name = data["Name"]
    cursor.close()
    cnx.close()
    return name
    
def build(onlyToday = True, execution_index = "1"):    
      #{chamada : link}
      test_data = returnTest(onlyToday, execution_index)
      chamadas = test_data.keys()
      chamadas_db = []
      cnx = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database = settings.database, charset = settings.charset)
      cursor = cnx.cursor(pymysql.cursors.DictCursor)
      cursor.execute("SELECT Title FROM news;")     
      data = cursor.fetchall()    
      for d in data:
        chamadas_db.append( d["Title"] )
      cursor.close()
      cnx.close()
      for c in chamadas:
          if c in chamadas_db:
            #chamada = news_dic.keys()[news_dic.values().index(c)]
            del test_data[c]
            print(" ->" + colored.red("Noticia ja existe:") + c)
          else:
            continue

      if len(test_data) > 0:
        expected_news = len(test_data)
        numero_execucoes = expected_news / 6
        executions_spare = expected_news % 6 

        #{chamada : categoria}
        count = 0
        news_dic = {}
        for p in range(numero_execucoes + 1):
            try:
                dic = ai.isaac.classify(test_data.keys()[count:count+6], "../../../public/container_folder")
                print("Linha 136:" + colored.blue(dic))
                for k, v in zip(dic.keys(), dic.values()):
                    news_dic[v] = k 
                count += 6
            except:
                continue    
        print("Tamanho do test_data:" + colored.yellow(str(len(test_data))))
        print("Tamanho do news_dic:" + colored.yellow(str(len(news_dic))))

        if executions_spare > 0:
            for o in range(executions_spare + 1):
                try:
                    dic = ai.isaac.classify(test_data.keys()[count:count+1], "../../../public/container_folder")
                    for k, v in zip(dic.keys(), dic.values()):
                        news_dic[v] = k 
                    count += 1
                except:
                    continue
        
        #/var/www/html/zogue/app/public/xml/pages
        #print("185)Antes de gambiara_dic:" + colored.blue(os.path.dirname(os.path.abspath(__file__))) )
        gambiara_dic = {'ciencia':1, 'economia':2, 'entretenimento':3, 'esportes':5, 'politica':4, 'tecnologia':6}    
        cnx = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database = settings.database, charset = settings.charset)
        cursor = cnx.cursor()
        for k in news_dic.keys():
                    try:
                        #image_number = randint(0,9)
                        #image_name = str(news_dic[k]) + "_" + str(image_number) + ".jpg"
                        text = "Lorem ispun lorem ispun lorem ispun lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun lorem ispun lorem ispun lorem ispun lorem ispun lorem ispun  lorem ispun  lorem ispun lorem ispun lorem ispun lorem ispun lorem ispun lorem ispun lorem ispun lorem ispun lorem ispun lorem ispun lorem ispun lorem ispun  lorem ispun  lorem ispun  lorem ispun  lorem ispun lorem ispun"
                        #Reconhecer entidades e salvar no banco de dados na coluna 'people_ner', 'organizations_ner'
                        dic = miner.get_entities(k)
                        people_list = []
                        org_list = []
                        if dic:
                            for c in dic:
                                if c == "people":
                                    if dic.get(c):
                                        #print(colored.green(dic.get(c)))
                                        for x in dic.get(c):
                                            people_list.append(x)
                                    else:
                                        continue
                                elif c =="organizations":
                                    if dic.get(c):
                                        #print(colored.green(dic.get(c)))
                                        for x in dic.get(c):
                                            org_list.append(x)
                                    else:
                                        continue
                                else:
                                    continue
                        people_list = ','.join(people_list)
                        org_list = ','.join(org_list)            
                        subcategory_id = miner.discover_subcategory(k, gambiara_dic[news_dic[k]] )
                        add_new = ("INSERT INTO news (Title, Text, Url, category_id, Author, image_file_name , image_content_type, image_file_size, image_updated_at, created_at, updated_at, news_edition, execution_index, already_known, people_list, organizations_list, subcategory_id) VALUES (%s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)")
                        new_data = (k, text, test_data[k], gambiara_dic[news_dic[k]] , "Ana", "", "image/jpeg", 47520, datetime.now(), datetime.now(), datetime.now(),  time.strftime("%Y-%m-%d"), execution_index, False, people_list, org_list, subcategory_id)
                        cursor.execute(add_new, new_data)
                        cnx.commit()
                        subcategory_name = returnSubcategory(subcategory_id)
                        print("->Chamada:" + colored.blue(k))
                        print("  Categoria:" + colored.red(news_dic[k]))
                        print("  Subcategoria:" + colored.red(subcategory_name))
                        print("  Link:" + colored.green(test_data[k]))
                        print("  Entidades:" + colored.blue(str(dic)))
                    except:
                        continue
        cursor.close()
        cnx.close()
        #/var/www/html/zogue/app/public/xml/pages
        os.chdir("../../../script/python")
      else:
          print(colored.red("Nao ha noticia para adicionar!"))  
          ###os.chdir("../../../script/python")
          os.chdir("../../../script/python")

if __name__ == "__main__":
    settings.init("development")
    build(True, "2")
    
    #validates_trainset()
    #test_data = returnTest(True)
    # ai.multiclass_classifier.classify(test_data.keys(),  "../../../public/container_folder")