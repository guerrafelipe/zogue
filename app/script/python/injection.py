#Abre o arquivo index-dd:mm:yyyy(1)(Z).html
#Aplica no arquivo html o codigo javascript na tag <head></head>
#Roda o site em uma das duas opcoes: 1)modo visivel 2)modo background
#Manda os dados via json para o django

# -*- encoding: utf-8 -*-
from lxml import html
import requests
import os, sys
import glob
import shutil
import sched, time
from datetime import datetime
from clint.textui import colored 
import re
import xml.dom.minidom
from xml.dom.minidom import parse
import xml.etree.ElementTree as ET 
import urllib2
import socket

#only use bs4
from bs4 import BeautifulSoup as bs

#TODO COMECO DE ARQUIVO RODAR UM METODO QUE CHECA A INTEGRIDADE DAS PASTAS E DOS ARQUIVOS

#Set the encoding
reload(sys)
sys.setdefaultencoding('UTF8')


"""
=======================================================================================================
=                                                                                                     =
=                       BEGIN                                                                         =   
=                                                                                                     =
========================================================================================================
"""

def inject(onlyToday = True, execution_index = "1"):
  #Get the JavaScript Content- MODIFICAO:
  main = open("../javascript/main.js","rb")
  js = main.read()
  main.close()

  #Set correct path
  path = "../../public/pages"
  os.chdir(path)
  folders_list = []
  folders_list = glob.glob("*")

  html0 = ""
  for f in folders_list:
    print("->Aplicando codigo JavaScript:" + colored.blue(f))
    os.chdir(f)
    files_list = []
    
    if onlyToday == True:
      files_list = glob.glob("*" + time.strftime("%d:%m:%Y") + "(" + execution_index + ")" + ".html")
    
    else:
      files_list = glob.glob("*.html")

    for g in files_list:
      try:
        #Le
        file1 = open(g, "rb")
        html0 = file1.read()
        file1.close()
      
        #Escreve
        file2 = open(g, "wb")
        soup = bs(html0, "lxml")
      
        [t.extract() for t in soup.findAll('title')]   

        script = soup.new_tag("script")
        script.string = js
        script.attrs['type'] = 'text/javascript'
      
        """
        <script   src="https://code.jquery.com/jquery-2.2.3.min.js"   integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo="   crossorigin="anonymous"></script>  
        """

        #Insere CDN do jQuery
        scriptjq = soup.new_tag("script")
        scriptjq.attrs['src'] = 'https://code.jquery.com/jquery-2.2.3.min.js'
        scriptjq.attrs['integrity'] = 'sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo='
        scriptjq.attrs['crossorigin'] = 'anonymous'
      
        id1 = g[:-5] 
        #Nome do site atual
        name = soup.new_tag("title")
        name.attrs["id"] = id1
        name.string = f

        #Adiciona a load_section
        load_section = soup.new_tag("div")
        load_section.string = "AJAX INICIA EM 5s"
        load_section.attrs["id"] = "loadSection"
        load_section.attrs["style"] = "color:white; background-color:black; font-size:20px; text-align:center;"

        #Lida mal com documentos vazios e com documentos que nao possuem as propriedades usadas no seletor jquery a > : header e :header > a
        soup.html.head.insert(0, scriptjq)
        soup.html.head.insert(1, script)    
        soup.html.head.insert(2, name )
        soup.html.body.append(load_section)
      
        #Para usar o canvas, garantir que o documento aberto eh HTML5
        file2.write("<!DOCTYPE html> <meta charset='utf-8' />"  + soup.prettify())
        file2.close()
      except:
        continue

    print(colored.green("  Codigo aplicado"))
    os.chdir("..")
    

  """
  LISTA LEGAL QUE MOSTRA TODA A ESTRUTURA DE ARQUIVOS NA LINHA DE COMANDO:
  for f in folders_list:
    os.chdir(f)
    files_list = []
    files_list = glob.glob("*.html")
    print(colored.red(">" + f))
    for g in files_list:
      print("-" + g)
    os.chdir("..")
  """  
  os.chdir("../../script/python")

if __name__ == "__main__":
  inject()
  