# -*- encoding: utf-8 -*-
from lxml import html
import requests
import os, sys
import glob
import shutil
import sched, time
from datetime import datetime
from clint.textui import colored
import re
import xml.dom.minidom
from xml.dom.minidom import parse
import xml.etree.ElementTree as ET 
import urllib2
from BeautifulSoup import BeautifulSoup as bs

#MUDA O ENCODING
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')


def total_clear():

	#Remove os arquivos XML que restaram na pasta ../php
	xml_files = glob.glob("../php/*.xml")
	for x in xml_files:
		os.remove("../php/" + x)
	print("___________________________________________")
	print("Arquivos XML da pasta" + colored.red(" script/php ") + "removidos")
	print("___________________________________________")


	if os.path.isdir("../../public/pages"):
		#Remove a pasta public/pages
		print("___________________________________")
		shutil.rmtree("../../public/pages")
		print("Pasta removida:" + colored.red("public/pages"))

		#Cria a pasta pages
		os.mkdir("../../public/pages")
		print("Pasta criada:" + colored.green("public/pages"))
		print("___________________________________")

	else:
		print("Essa pasta ja nao existe" + colored.green(":)") )

	if os.path.isdir("../../public/xml/pages"):
		#Remove a pasta public/xml/pages
		print("___________________________________")
		shutil.rmtree("../../public/xml/pages")
		print("Pasta removida:" + colored.red("public/xml/pages"))

		#Cria a pasta pages
		os.mkdir("../../public/xml/pages")
		print("Pasta criada:" + colored.green("public/xml/pages"))
		print("___________________________________")

	
def partial_clear(onlyToday = True, execution_index = 2):
	#Remove os arquivos XML que restaram na pasta ../php
	xml_files = []
	if onlyToday == True:	
		xml_files = glob.glob("../php/" + "*" + time.strftime("%d:%m:%Y") + "(" + str(execution_index) + ")" + ".xml")
	else:
		xml_files = glob.glob("../php/*.xml")

	if xml_files:
		for x in xml_files:
			print("___________________________________________")
			os.remove("../php/" + x)
			print("Arquivo XML da pasta" + colored.red(" script/php ") + "removido")
			print("___________________________________________")
	else:
		print(colored.green("Nao ha arquivos XML na pasta ../php"))

	#.html
	os.chdir("../../public/pages")
	folders_list1 = glob.glob("*")
	if folders_list1:
		for f in folders_list1:
			os.chdir(f)
			if onlyToday == True:
				files_list1 = glob.glob("*" + time.strftime("%d:%m:%Y") + "(" + str(execution_index) +")" + ".html")
			else:
				files_list1 = glob.glob("*.html")
			
			for g in files_list1:
				os.remove(g)	
			os.chdir("..")
	else:
		print(colored.green("Nao ha arquivos HTML na pasta ../../public/pages"))

	#.xml
	os.chdir("../xml/pages")
	folders_list2 = glob.glob("*")
	if folders_list2:
		for f in folders_list2:
			os.chdir(f)
			if onlyToday == True:
				files_list2 = glob.glob("*" + time.strftime("%d:%m:%Y") + "(" + str(execution_index) + ")" + ".xml")
			else:
				files_list2 = glob.glob("*.xml")
			
			for g in files_list2:
				os.remove(g)	
			os.chdir("..")
	else:
		print(colored.green("Nao ha arquivos XML na pasta ../xml/pages"))

	os.chdir("../../../script/python")
	print(colored.green("Limpeza realizada com sucesso"))
	#print("PASTA DO PROCESSO 7:" + colored.blue(os.path.dirname(os.path.abspath(__file__))))

if __name__ == "__main__":
	print("_________________________________________________")
	print("Bem-vindo ao apocalipse. Selecione uma opcao:")
	print("1)Limpeza completa - exclui pastas e arquivos")
	print("2)Limpeza parcial - mantem pastas e excluir arquivos")
	print("_________________________________________________")
	escolha = raw_input(colored.red("RESPOSTA:"))

	if escolha ==  "1":	
		total_clear()

	elif escolha == "2":
		partial_clear()

	else:
		print("Essa opcao nao existe :(")
