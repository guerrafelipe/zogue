import genesis, injection, settings, runner, mover, builder, container_manager, apocalipse, statistics, miner, reviewer
import time
from datetime import datetime
from xml.dom.minidom import Document
import xml.dom.minidom
from xml.dom.minidom import parse
import xml.etree.ElementTree as ET 
import socket
import ai.isaac
from clint.textui import colored
import os, sys, glob
import pymysql
import pymysql.cursors
import traceback
from multiprocessing import Pool

"""
==============================================================================
=                                                                            =
=  Bem-vindo ao Integration. Esse script possibilita que todos os passos     =
=  sejam executados de maneira autonoma, so pegar uma xicara de cafe e esperar!
=  executar somente as noticias de hoje!
==============================================================================
"""

"""
->Saber qual o numero da execucao que esta sendo feita para evitar que o injection, runner
e builder lidem com o mesmo arquivo mais de uma vez por dia
 ... Contar o numero de linhas da tabela statistics onde o created_at seja igual a hoje
 ... Garantir que existem arquivos com o dado indice
->Usar o titulo da noticia como uma especie de chave primaria, dessa maneira duas noticias
iguais nunca serao exibidas no noticiario
"""
#Get the argument and pass to settings.py
#python test.py arg1 arg2 arg3 -> [test.py, arg1, arg2, arg3]
env = sys.argv[1] #production or development
settings.init(env)
mode = sys.argv[2] #nobreak or break
driver = sys.argv[3] #firefox or chrome

if sys.argv[1] == "production":
    print("AMBIENTE:" + colored.red("PRODUCAO"))
else:
    print("AMBIENTE:" + colored.red("DESENVOLVIMENTO"))

print("HOST:" + colored.red(settings.host))

if sys.argv[3] == "chrome":
    print("DRIVER:" + colored.red("CHROME"))
else:
    print("DRIVER:" + colored.red("FIREFOX"))
print("Quantas vezes vc deseja executar o script?")
ntimes = raw_input("RESPOSTA:")
ntimes = int(ntimes)
count = 0

while count < ntimes:
    cnx = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database = settings.database, charset = settings.charset)
    cursor = cnx.cursor(pymysql.cursors.DictCursor)   
    cursor.execute("SELECT * FROM statistics WHERE DATE(created_at) = '%s';" % time.strftime("%Y:%m:%d") )     
    data = cursor.fetchall()
    execution_index = len(data) + 1
    execution_index = str(execution_index)

    print("______________________________________________________________")
    print(colored.green("BEM-VINDO AO INTEGRATION"))
    print("Execucao diaria numero: " + colored.blue(execution_index))
    print("______________________________________________________________")
    cursor.close()
    cnx.close()

    process_duration = []
    """
    ======================================================================
                                .:PROCESSO 0:.
                    MONTA A ESTRUTURA DAS PASTAS E FAZ SCRAP - GENESIS
    ======================================================================
    """
    print("________________________________________________________")
    print(colored.green("SCRIPT ATUAL: GENESIS"))
    t0 = time.clock()

    #Verifica se existe a pasta ../../public/pages
    if os.path.isdir("../../public/pages"):
        os.chdir("../../public/pages")
    else:
        os.mkdir("../../public/pages")
        os.chdir("../../public/pages")
    
    #Pega a lista de sites do 'test set'
    cnx = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database = settings.database, charset = settings.charset)
    cursor = cnx.cursor(pymysql.cursors.DictCursor)   
    cursor.execute("SELECT Name, Url FROM sites WHERE Model = 'test';")     
    data = cursor.fetchall()

    for d in data:
        if d is not None:
            nome = d["Name"]
            url = d["Url"]

            try:
                print("->Site baixado:" + colored.blue(nome))
                print("  URL:" + colored.blue(url))
                
                if os.path.isdir(nome):
                    os.chdir(nome)
                else:
                    os.mkdir(nome)
                    os.chdir(nome)
                genesis.scrap(nome, url, "", execution_index)
                os.chdir("..")

            except Exception, errormsg:
                """
                ->Colocar algo aqui quando houver alguma falha em baixar o site
                ->Se der errado, simplesmente pular o site e ir para o proximo
                """
                os.chdir("..")
                #Tentar mais uma vez com outro proxy antes de mostrar a mensagem
                #apocalipse.partial_clear(True, execution_index)
                print "Error message: %s" % errormsg
                traceback.print_exc()
                continue
    cursor.close()
    cnx.close()
    
    #Pega a lista de sites do 'test set'
    cnx = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database = settings.database, charset = settings.charset)
    cursor = cnx.cursor(pymysql.cursors.DictCursor)   
    cursor.execute("SELECT Name, Url, category_id FROM sites WHERE Model = 'train';")     
    data = cursor.fetchall()
    gambiara_dic = {1 : 'ciencia', 2 : 'economia', 3 : 'entretenimento', 5 : 'esportes', 4 : 'politica', 6 : 'tecnologia'}     
    for d in data:
        if d is not None:
            nome2 = d["Name"]
            url2 = d["Url"]
            category_id = d["category_id"]
            category = gambiara_dic[category_id]
            try:
                print("->Site baixado:" + colored.blue(nome2))
                print("  URL:" + colored.blue(url2))
                
                if os.path.isdir(nome2):
                    os.chdir(nome2)
                else:
                    os.mkdir(nome2)
                    os.chdir(nome2)
                genesis.scrap(nome2, url2, category, execution_index)
                os.chdir("..")

            except Exception, errormsg:
                """
                Hello World
                """
                os.chdir("..")
                print "Error message: %s" % errormsg
                traceback.print_exc()
                continue
    cursor.close()
    cnx.close()
    tf = time.clock()
    dt = tf - t0
    process_duration.append(dt)
    
    os.chdir("../../script/python")
    print("________________________________________________________")
    """
    ======================================================================
                                .:PROCESSO 1:.
                        APLICA O JAVACRIPT AS PAGINAS - INJECTION
    ======================================================================
    """
    print("________________________________________________________")
    print(colored.green("SCRIPT ATUAL: INJECTION"))

    #1- APLICA O CODIGO
    t0 = time.clock()
    injection.inject(execution_index)
    tf = time.clock()
    dt = tf - t0
    process_duration.append(dt)
    
    print("________________________________________________________")

    """
    ======================================================================
                                .:PROCESSO 2:.
                    EXECUTA O NAVEGADOR NO BACKGROUND - RUNNER
    ======================================================================
    """
    
    """
    Usar das threads:
    1)Roda os sites do 'test set'
    2)Roda os sites do 'train set'
    """
    print("________________________________________________________")
    print(colored.green("SCRIPT ATUAL: RUNNER"))
    t0 = time.clock()

    os.chdir("../../public/pages")
    folders_list = glob.glob("*")
    categories = ["ciencia", "economia", "entretenimento", "politica", "esportes", "tecnologia"]    
    test_folders = []
    train_folders = []
    for f in folders_list:
        categorie = f.split("_")[0]
        if categorie in categories:    
            train_folders.append(f)
        else:
            test_folders.append(f)

    runner.run(train_folders, execution_index, driver)
    runner.run(test_folders, execution_index, driver)
    
    tf = time.clock()
    dt = tf - t0
    process_duration.append(dt)
    print("________________________________________________________")

    """
    ======================================================================
                                .:PROCESSO 3:.
                Realoca os arquivos XML gerados pelo runner.py - MOVER
    ======================================================================
    """
    
    t0 = time.clock()
    mover.move(execution_index)
    tf = time.clock()
    dt = tf - t0
    process_duration.append(dt)
    
    """
    ======================================================================
                                .:PROCESSO 4:.
            Alimenta a pasta container_folder com vocabulario do dia
    ======================================================================
    """ 
    #Evitar que a pasta container_manager seja alimentada mais de uma vez ao dia
    if execution_index == "1":
        #print(os.path.dirname(os.path.abspath(__file__)))  
        t0 = time.clock()   
        container_manager.manage(time.strftime("%d:%m:%Y"), execution_index)
        tf = time.clock()
        dt = tf - t0
        process_duration.append(dt)
    else:
        process_duration.append( 0.0 )
        print("Execucao do container_manager ja foi feita hoje!")
    """
    ======================================================================
                                .:PROCESSO 5:.
                FAZ O TRABALHO DE INTELIGENCIA ARTIFICIAL - builder.py
    ======================================================================
    """
    t0 = time.clock()
    builder.build(True, execution_index)
    tf = time.clock()
    dt = tf - t0
    process_duration.append(dt)

    if mode == "break":
        #beep
        print "\a"
        raw_input("PRESSIONE UM BOTAO SE JA FEZ A VALIDACAO COMO ADMIN...")
        time.sleep(3)

        """
        ======================================================================
                                .:PROCESSO 5:.
          FAZ A ANALISE DE EFICIENCIA DO PROCESSO - statistics.py
        ======================================================================
        """

        #print(colored.blue(os.path.dirname(os.path.abspath(__file__))))
        t0 = time.clock()
        statistics.performance(count, True, execution_index, False)
        tf = time.clock()
        dt = tf - t0
        process_duration.append(dt)
    
    else:
        t0 = time.clock()
        statistics.performance(count, True, execution_index, True)
        tf = time.clock()
        dt = tf - t0
        process_duration.append(dt)

    """
    ======================================================================
                                .:PROCESSO 6:.
                FAZ A REVISAO DE NOTICIAS MODIFICADAS
    ======================================================================
    """
    print("________________________________________________________")
    print(colored.green("SCRIPT ATUAL: REVIEWER"))
    reviewer.review()
    
    print("________________________________________________________")

    print("______________________________________________________")
    print(colored.red("ANALISE DO TEMPO DE EXECUCAO DE CADA ETAPA"))
    print("Genesis: %.2fs"  %(process_duration[0]))
    print("Injection: %.2fs" %(process_duration[1]))
    print("Runner: %.2fs"  %(process_duration[2]))
    print("Mover: %.2fs"  %(process_duration[3]))
    print("Container Manager: %.2fs"  %(process_duration[4]))
    print("Builder: %.2fs" %(process_duration[5]))
    print("______________________________________________________")
    count += 1

"""
======================================================================
                                .:PROCESSO 8:.
FAZ A RECICLAGEM DOS ARQUIVOS .html e .xml extraidos na execucao
======================================================================
"""
print("ANTES DA RECICLAGEM:" + colored.blue(os.path.dirname(os.path.abspath(__file__))))
print("Aperte uma tecla para fazer a reciclagem...")    
apocalipse.partial_clear(True, execution_index)
