# -*- encoding: utf-8 -*-
import pymysql
import pymysql.cursors
from clint.textui import colored
import os, sys
import nltk
from nltk.tokenize import PunktSentenceTokenizer, word_tokenize
from nltk.corpus import machado
from nltk.tag import StanfordNERTagger
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import style
from nltk import pos_tag
from nltk.chunk import conlltags2tree
from nltk.tree import Tree
import wikipedia
from nltk.corpus import stopwords
from bs4 import BeautifulSoup
from urllib2 import urlopen
import urllib
import settings
import ai.subclass_classifier

#MUDA O ENCODING
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')

"""
Das noticias ja validades pelos 'usuarios vip' extrair algumas caracteristicas, como
nome de pessoas 
"""

def returnSubcategories(category_id):
	#{subcategory_id : description}
	subcategories = {}
	cnx = pymysql.connect(host = settings.host, user = settings.user, password = settings.password, database = settings.database, charset = settings.charset)
	cursor = cnx.cursor(pymysql.cursors.DictCursor)   
	cursor.execute("SELECT id, Description  FROM subcategories WHERE category_id = '%s';" % str(category_id))     
	data = cursor.fetchall()
	for d in data:
		if d is not None:
			subcategory_id = d["id"]
			description = d["Description"]
			subcategories[subcategory_id] = description
	cursor.close()
	cnx.close()
	return subcategories
	
# Tag tokens with standard NLP BIO tags
def bio_tagger(ne_tagged):
		bio_tagged = []
		prev_tag = "O"
		for token, tag in ne_tagged:
			if tag == "O": #O
				bio_tagged.append((token, tag))
				prev_tag = tag
				continue
			if tag != "O" and prev_tag == "O": # Begin NE
				bio_tagged.append((token, "B-"+tag))
				prev_tag = tag
			elif prev_tag != "O" and prev_tag == tag: # Inside NE
				bio_tagged.append((token, "I-"+tag))
				prev_tag = tag
			elif prev_tag != "O" and prev_tag != tag: # Adjacent NE
				bio_tagged.append((token, "B-"+tag))
				prev_tag = tag
		return bio_tagged

# Create tree       
def stanford_tree(bio_tagged):
	tokens, ne_tags = zip(*bio_tagged)
	pos_tags = [pos for token, pos in pos_tag(tokens)]

	conlltags = [(token, pos, ne) for token, pos, ne in zip(tokens, pos_tags, ne_tags)]
	ne_tree = conlltags2tree(conlltags)
	return ne_tree


# Parse named entities from tree
def structure_ne(ne_tree):
	ne = []
	for subtree in ne_tree:
		if type(subtree) == Tree: # If subtree is a noun chunk, i.e. NE != "O"
			ne_label = subtree.label()
			ne_string = " ".join([token for token, pos in subtree.leaves()])
			ne.append((ne_string, ne_label))
	return ne

def get_entities(title):
	train_corpus = machado.raw('romance/marm05.txt')
	st = StanfordNERTagger('../../stanford-ner/classifiers/english.all.3class.distsim.crf.ser.gz',
						   '../../stanford-ner/stanford-ner.jar',
						   encoding='utf-8')
	tokenized_text = word_tokenize(title)
	classified_text = st.tag(tokenized_text)
	#List of tuples [('A', 'PERSON'), ('B', 'ORGANIZATION')]
	entities_list = structure_ne(stanford_tree(bio_tagger( classified_text )))
	pt_stopwords = stopwords.words('portuguese')
	result_dic = {}
	result_dic["people"] = []
	result_dic["organizations"] = []
	for t in entities_list:
		try:
			index_t = entities_list.index(t)
			if t[1] == "PERSON":
				name = t[0]
				name_splited = name.split()
				for n in name_splited:
					n = n.lower()
				f_name = name_splited[0]
				for h in name_splited:
					if h in pt_stopwords:
						entities_list.remove(index_t)
						raise GetOutOfLoop
					else:
						continue
				result_dic["people"].append(name)
			elif t[1] == "ORGANIZATION":
				name = t[0]
				name_splited = name.split()
				for n in name_splited:
					n = n.lower()
				for h in name_splited:
					if h in pt_stopwords:
						entities_list.remove(index_t)
						raise GetOutOfLoop
					else:
						continue
				result_dic["organizations"].append(name)
			else:
				continue
		except GetOutOfLoop:
			continue

	#{ people : [], locations : [], organizations : [] }
	return result_dic

def discover_subcategory(title, category_id):
	subcategories = returnSubcategories(category_id)
	if subcategories:
		similarities = []
		for s in subcategories.values():
			similarity = ai.subclass_classifier.cosine_sim(title, s)
			similarities.append(similarity)
		max_item = max(similarities)
		max_item_index = similarities.index(max_item)
		for d, k in zip(range(max_item_index + 1), subcategories.keys()):
			if d == max_item_index:
				subcategory_id = k
			else:
				continue
		return subcategory_id
	else:
		return 0

if __name__ == "__main__":
	get_entities("Hello")