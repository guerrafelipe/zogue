# -*- encoding: utf-8 -*-
import requests
import os, sys
import glob
import shutil
import sched, time
from datetime import datetime
from clint.textui import colored
import xml.dom.minidom
from xml.dom.minidom import parse, parseString
from bs4 import BeautifulSoup
import ai.isaac
import io
from time import gmtime, strftime
import json
import mysql.connector
import urllib2
from urllib2 import urlopen
from bs4 import BeautifulSoup as bs

#MUDA O ENCODING
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')


"""
->Agir depois do mover.py e antes do builder.py
->Acessa o link de cada notica e extrai o corpo
do texto
->Se o link nao funcionar, ja descartar aqui evitando que ele 
apareca na pagina do noticiario
"""


def getText(onlyToday = False):
     os.chdir("../../public/xml/pages")    
     folders = glob.glob("*")
     for f in folders:
        os.chdir(f)
        
        if onlyToday == True:
          files = glob.glob("*" + time.strftime("%d:%m:%Y") + "*.xml")
        
        else:
          files = glob.glob("*.xml")
        
        if len(files) > 0:        
            for g in files:
                    file1 = io.open(g, encoding="utf8")
                    content = file1.read()
                    file1.close()
                    
                    DOMTree = xml.dom.minidom.parseString(content)
                    noticiario = DOMTree.documentElement
                    noticias = noticiario.getElementsByTagName("noticia")
                    
                    for s in noticias:
                        count_n = len(s.getElementsByTagName("chamada"))
                        count_u = len(s.getElementsByTagName("link"))
                       
                        if count_n == count_u:
                            n = s.getElementsByTagName("chamada")[0]
                            u = s.getElementsByTagName("link")[0]
                            try: 
                                chamada_texto = n.childNodes[0].data
                                chamada_texto = chamada_texto.strip()
                                
                                chamada_link =  u.childNodes[0].data
                                chamada_link = chamada_link.strip()
                                
                                if chamada_texto and chamada_link:
                                    chamada_texto = chamada_texto.replace("\n","")                                    
                                    chamada_texto = chamada_texto.replace("\t","")
                                    spl = chamada_texto.split()
                                                                       
                                    if len(spl) > 3:
                                        chamada_texto = str(chamada_texto)
                                        chamada_link = str(chamada_link)
                                        chamada_texto = chamada_texto.encode("utf8")
                                        chamada_link = chamada_link.encode("utf8")                                        
                                        
                                        response = urllib2.urlopen(chamada_link)
                                        html = response.read()
                                        soup = bs(html, "lxml")
                                        
                                        for par in soup.find_all('p'): 
                                            """
                                            ->
                                            IDEIA1: Um bom indicativo de onde esta 
                                            o corpo do texto eh a densidade
                                            de palavras e de elementos que 
                                            geralmente representam texto (headers, 
                                            span, p...). densidade superficial de palavras
                                            pode ser obtida pelo numero de palavras pela 
                                            area do elemento em px^2
                                            IDEIA2: Ja que ja tenho a manchete 
                                            ->Para cada p verificar se ha um ou
                                            mais p no mesmo nivel e juntar o texto
                                            se este for valido.
                                            
                                            WEIRD- <article> com varios <p> e <div> dentro
                                            """
                                            
                                            if len(par.next_siblings) > 0:    
                                                text = ""                                                
                                                for sibl in par.next_siblings:
                                                    if sibl.name == "p":
                                                        text += sibl.get_text()
                                                text = text.strip()
                                                print(text)
                                                
                                            else:
                                                
                                                text = par.get_text() 
                                                text = text.strip()
                                                if text:
                                                    if len(text) > 3:
                                                        print(text)
                                        
                                        #append a tag called texto
                                        
                                        
                            except:
                                continue
        os.chdir("..")
    #Retorna um dicionario {chamada : link}

if __name__ == "__main__":
	getText(True)
