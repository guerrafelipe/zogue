# -*- encoding: utf-8 -*-

import nltk, string
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import sys, os
import pymysql
from clint.textui import colored
reload(sys)
sys.setdefaultencoding('UTF8')

"""
http://stackoverflow.com/questions/8897593/similarity-between-two-text-documents

https://janav.wordpress.com/2013/10/27/tf-idf-and-cosine-similarity/

http://graus.co/thesis/string-similarity-with-tfidf-and-python/
"""

stemmer = nltk.stem.porter.PorterStemmer()
remove_punctuation_map = dict((ord(char), None) for char in string.punctuation)

def stem_tokens(tokens):
    return [stemmer.stem(item) for item in tokens]

'''remove punctuation, lowercase, stem'''
def normalize(text):
    return stem_tokens(nltk.word_tokenize(text.lower().translate(remove_punctuation_map)))

vectorizer = TfidfVectorizer(tokenizer=normalize)

def cosine_sim(text1, text2):
	text1 = word_tokenize(text1)
	text2 = word_tokenize(text2)
	for sw in stopwords.words("portuguese"):
		if sw in text1:
			text1.remove(sw)
		else:
			pass

        if sw in text2:
        	text2.remove(sw)
        else:
        	pass
	text1 = ' '.join(text1)
	text1 = text1.strip()    
	text2 = ' '.join(text2)
	text2 = text2.strip()   
	tfidf = vectorizer.fit_transform([text1, text2])
	return ((tfidf * tfidf.T).A)[0,1]
