# -*- encoding: utf-8 -*-
import os, sys
from datetime import datetime
from clint.textui import colored
import nltk
#from nltk.book import *
from nltk.collocations import *
from nltk.collocations import BigramCollocationFinder
from nltk.metrics import BigramAssocMeasures
import urllib
import requests
from nltk.classify.scikitlearn import SklearnClassifier
from nltk.classify import ClassifierI
import random
from nltk.corpus import stopwords
import io
import sklearn
import sklearn.datasets
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics
import numpy as np
from sklearn.externals import joblib
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.pipeline import Pipeline
from time import strftime
import time
from nltk.book import *
try:
    from itertools import izip as zip
except ImportError: # will be 3.x series
    pass

#MUDA O ENCODING
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')

#Pega os arquivos XML na pasta ../../public/xml/pages/[NOME] e faz as analises
#In python, empty strings are considered 'falsy'

"""
=======================================================================================================
=																		      =
=												BEGIN							 =		
=																			 =
=======================================================================================================
"""

"""
USING Multinomial Naive Bayes-> Works                              
"""

#http://scikit-learn.org/stable/auto_examples/text/document_classification_20newsgroups.html
#http://blog.datumbox.com/using-feature-selection-methods-in-text-classification/
#http://nlp.stanford.edu/IR-book/html/htmledition/text-classification-and-naive-bayes-1.html

def classify(test_data, path, execution_index = "1"):
    print("test_data que chega ao isaac:" + colored.yellow(str(len(test_data))))
    #TRAIN DATA    
    train_data = sklearn.datasets.load_files(path, load_content=True, shuffle=True, encoding="utf-8")
    count_vect = CountVectorizer()
    X_train_counts = count_vect.fit_transform(train_data.data)
    tfidf_transformer = TfidfTransformer()
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)    
    if os.path.isdir("joblib.pkl") == False:
        os.mkdir("joblib.pkl")
    filename = 'joblib.pkl/' + time.strftime("%d:%m:%Y") +'.joblib.pkl'
    if execution_index == "1":
        t01 =  time.time()  
        clf = MultinomialNB().fit(X_train_tfidf, train_data.target)
        tf1 =  time.time()
        train_time = tf1 - t01 #tempo de treinameinto
        #Compress eh diretamente proporcional ao tempo de salvamento
        joblib.dump(clf, filename, compress = 0)
    else:
        t01 = time.time()
        clf = joblib.load(filename)
        tf1 = time.time()
        train_time = tf1 - t01

    #TEST DATA
    X_new_counts = count_vect.transform(test_data)
    X_new_tfidf = tfidf_transformer.transform(X_new_counts)
    t02 = time.time()
    predicted = clf.predict(X_new_tfidf)
    tf2 = time.time() 
    test_time = tf2 - t02
    #mettricas.accuracy_score(y_true, y_predicted)    
    #score = metrics.accuracy_score(train_data.target_names, train_data.data)
    #score = metrics.accuracy_score(test_data, predicted)
    #score = clf.score(train_data.data, train_data.target)

    #print("____________________________________________________________")    
    #print(colored.red("ANTES DE COMECAR, VEJA O RELATORIO do isaac.py"))
    #print("Algoritmo de classificao:" + colored.blue("MultinomialNB"))    
    #print("Tempo de teste:" + colored.blue(str(test_time)))
    #print("Tempo de treino" + colored.blue(str(train_time)))
    #print("Pontuacao:" + colored.blue(str(score)))    
    #print("____________________________________________________________")   
    
    """
    Zip -> iterates over two lists together
    """
    news_classified = {}
    for doc, category in zip(test_data, predicted):
          news_classified[train_data.target_names[category]] = doc    
    return news_classified
    
if __name__ == "__main__":
    clf = classify("colocar aqui o teste")
    print(clf)    


"""
train_data = sklearn.datasets.load_files("../../../public/container_folder", load_content=True, shuffle=True, encoding="utf-8")
test_data = ["25 filmes para ver no Dia das Mães na Netflix", " Quem são os fiéis escudeiros que acompanham Temer"]

text_clf = Pipeline([('vect', CountVectorizer()),
                      ('tfidf', TfidfTransformer()),
                      ('clf', SGDClassifier(loss='hinge', penalty='l2',
                                            alpha=1e-3, n_iter=5, random_state=42)),
 ])
_ = text_clf.fit(train_data.data, train_data.target)
predicted = text_clf.predict(test_data)
#np.mean(predicted == twenty_test.target)
for doc, category in zip(test_data, predicted):
     print('%r => %s' % (doc, train_data.target_names[category]))            


   #Salvar a precisao e os tempos de treino e teste num documento XML    
    
        <classification>
            <time>YY-MM-DD HH:MM:SS</time>
            <classifier_name></classifier_name>
            <train_time></train_time>
            <test_time></test_time>
            <score></score>
        </classification>
        
           top = Element("classification")
    time_now = SubElement(top, "time")
    time_now.text = strftime("%Y-%m-%d %H:%M:%S")    
    classifier_name = SubElement(top, "classifier_name")
    classifier_name.text = "MultinomialNB"
    ttime = SubElement(top, str(float64(train_time)))
    ttime.text = train_time
    tsttime = SubElement(top, str(float64(test_time))) 
    tsttime.text = test_time
    sc = SubElement(top, "score")    
    sc.text = score
    
    fileReport = file("../reports/classifier.xml", "wb")
    fileReport.write(tostring(top))
    fileReport.close()
    print(colored.green("REPORT ESCRITO COM SUCESSO!"))
    
    """ 