# -*- encoding: utf-8 -*-
import os, sys, io
import glob
import time
from clint.textui import colored
import xml.dom.minidom
from xml.dom.minidom import parse, parseString
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import nltk
import matplotlib
import numpy
import string
from string import maketrans   # Required to call maketrans function.

reload(sys)
sys.setdefaultencoding('UTF8')

def Filter(pre_texto):
    #pos_texto = pre_texto.translate(string.maketrans(string.punctuation, ""), string.punctuation)
    pos_texto = "".join(l for l in pre_texto if l not in string.punctuation)
    return pos_texto

def manage(date, execution_index = "1"):
    def returnCategories():
      #Get the categories
      DOMTree = xml.dom.minidom.parse("../../public/xml/sites_by_categorie.xml")
      sites = DOMTree.documentElement
      categorie = sites.getElementsByTagName("categoria")
      categories = []
      for c in categorie:
         categories.append(c.attributes["nome"].value)
      return categories
    print("___________________________________________________")
    print("SCRIPT ATUAL:" + colored.green("CONTAINER MANAGER"))
    categories = returnCategories()
    os.chdir("../../public")
    if os.path.isdir("container_folder"):
        os.chdir("container_folder")
    else:
        os.mkdir("container_folder")
        os.chdir("container_folder")
    categories_folder = glob.glob("*")
    for y in categories:
        if y in categories_folder:
            continue
        else:
            os.mkdir(y)
    os.chdir("../xml/pages")
    for c in categories:
        texto = ""
        folders_list1 = glob.glob(c + "_*")
        for f in folders_list1:
            os.chdir(f)
            files_list1 = glob.glob("*" + date + "(" + str(execution_index) + ")" +".xml")
            for g in files_list1:
                file1 = open(g, "rb")
                content = file1.read()
                file1.close()
                DOMTree2 = xml.dom.minidom.parseString(content)
                noticiario = DOMTree2.documentElement
                chamadas = noticiario.getElementsByTagName("chamada")            
                for x in chamadas:
                   pre_texto = x.childNodes[0].data
                   pos_texto = Filter(pre_texto) 
                   texto += pos_texto 
            os.chdir("..")    
        os.chdir("../../container_folder/" + c)    
        file2 = open(date, "wb")
        texto = word_tokenize(texto) 
        for sw in stopwords.words("portuguese"):
            if sw in texto:
                texto.remove(sw)
        texto = ' '.join(texto)
        texto = texto.strip()        
        file2.write(texto)
        file2.close()
        print("->Categoria:" + colored.green(c))
        print("  Local:" + colored.blue(os.path.dirname(os.path.abspath(__file__))))           
        os.chdir("../../xml/pages") 
    os.chdir("../../../script/python")
    print("___________________________________________________")
def analyser(date = "all"):
    os.chdir("../../public/container_folder")
    folders = glob.glob("*")
    for f in folders:
        txt = ""
        os.chdir(f)
        if date == "all":
            files = glob.glob("*")
        else:
            files = glob.glob(date + "*")
        for g in files:
            file1 = open(g, "rb")
            txt += file1.read()
            file1.close()
        txt = unicode(txt, errors='ignore')
        txt = txt.split()
        fdist = nltk.FreqDist(txt)
        print(colored.red(f))
        fdist.plot(50, cumulative = False)    
        os.chdir("..")

if __name__ == "__main__":
    manage(time.strftime("%d:%m:%Y"), 2)
    #analyser()