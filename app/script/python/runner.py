# -*- encoding: utf-8 -*-
import os, sys
from pyvirtualdisplay import Display
from selenium import webdriver
from lxml import html
import requests
import os, sys
import glob
import shutil
import sched, time
from datetime import datetime
from clint.textui import colored
import re
import xml.dom.minidom
from xml.dom.minidom import parse
import xml.etree.ElementTree as ET 
import urllib2
from BeautifulSoup import BeautifulSoup as bs
import socket

#MUDA O ENCODING
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')

#Executa cada pagina no navegador em background
#usando selenium: http://stackoverflow.com/questions/16180428/can-selenium-webdriver-open-browser-windows-silently-in-background
#Funcao que abre um navegador em background

def run(received_folder, execution_index = "1", driver = "chrome"):
    for f in received_folder:
        #print(colored.red(threadName) + os.path.dirname(os.path.abspath(__file__))) 
        os.chdir(f)
        files_list1 = glob.glob("*" + time.strftime("%d:%m:%Y") + "(" + execution_index + ")" + ".html")
        for g in files_list1:
             #.html     
             display = Display(visible=0, size=(800, 600))
             display.start()

             if driver == "chrome":
                os.environ["webdriver.chrome.driver"] = "/usr/bin/chromedriver"
                browser = webdriver.Chrome("/usr/bin/chromedriver")
             else:
                browser = webdriver.Firefox()
                
             browser.get("http://localhost/zogue/app/public/pages/" + f + "/" + g)
             print("->Criando o XML:" + colored.blue(browser.title))
             print("  URL:" + colored.blue("http://localhost/zogue/app/public/pages/" + f + "/" + g))
             time.sleep(5)
             browser.quit()
             display.stop()
             print(colored.green("  Arquivo XML criado com sucesso"))

             #except:
                #print(colored.red("  O arquivo XML nao pode ser criado"))
                #continue
        os.chdir("..")

"""     
if __name__ == "__main__":
    run()
"""