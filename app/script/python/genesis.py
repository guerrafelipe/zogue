#Inicia a estrutura do scrap
#Esse arquivo pega a lista de sites em XML, baixa via wget e coloca em suas respectivas pastas

# -*- encoding: utf-8 -*-
from lxml import html
import requests
import os, sys
import glob
import shutil
import sched, time
from datetime import datetime
from clint.textui import colored
import re
import xml.dom.minidom
from xml.dom.minidom import parse
import xml.etree.ElementTree as ET 
import urllib2
from urllib2 import urlopen
from bs4 import BeautifulSoup as bs
from xml.dom.minidom import Document
import socket
import random

#The important part is opening the file in binary mode - then response body can be written bytewise without decoding or encoding steps.

#MUDA O ENCODING
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')

"""
Ainda da pra melhorar:
-Usar nmap pra colocar um proxy e netcat pra testar a conexao
 http://stackoverflow.com/questions/3764291/checking-network-connection
-Limpar linhas em branco nos arquivos

->SE PRECISAR USAR UMA LISTA DE AGENTES:
https://pypi.python.org/pypi/fake-useragent
"""

#Abrir o arquivo xmlhandler.php
#Para cada pagina criar um script PHP correspondente, que vai criar o XML
#filex = open("../php/xmlhandler.php", "rb")
#php = filex.read()
#filex.close()

"""
=======================================================================================================
=																									  =
=												BEGIN												  =		
=																									  =
========================================================================================================
"""


def LoadUserAgents(uafile="../../../script/python/connection_policy/user_agent.txt"):    
    uas = []
    with open(uafile, 'rb') as uaf:
        for ua in uaf.readlines():
            if ua:
                uas.append(ua.strip()[1:-1-1])
    #Embaralhar
    random.shuffle(uas)
    return uas


"""
->USAR LISTA DESSE SITE
http://proxylist.hidemyass.com/
->Melhorar o algoritmo, atualizando o documento a cada 5segundos com IP confiaveis e + rapidos
"""
def LoadProxy(pfile = "../../../script/python/connection_policy/proxy.txt"):
	proxies = []
	with open(pfile, "rb") as pf:
		for p in pf.readlines():
				proxies.append(p)
	#Embaralhar
	random.shuffle(proxies)
	return proxies

def connect(url, anonymity = True ):
	if anonymity == True:
		uas = LoadUserAgents()
		ua = random.choice(uas)
		prox = LoadProxy()
		proxy_number = random.choice(prox)
		proxy = {"http": "http://" + str(proxy_number)}
		headers = {"Connection" : "close", "User-Agent" : ua}
		response = requests.get(url, proxies = proxy, headers = headers, timeout = 30)
		html = response.text
	
	else:
		response = requests.get(url)
		html = response.text
	
	return html

#Eh disparado em caso de erro de timeout, evitando 
def partial_scrap(sites_finalizados):
	nomes = []
	urls = []
	#Parse do arquivo ../../public/xml/sites.xml
	DOMTree = xml.dom.minidom.parse("../../xml/sites.xml")
	sites = DOMTree.documentElement
	site = sites.getElementsByTagName("site")
	for s in site:
		n = s.getElementsByTagName("nome")[0]
		u = s.getElementsByTagName("url")[0]
		nomes.append(n.childNodes[0].data)
		urls.append(u.childNodes[0].data)

	#Significa que o codigo parou logo no comeco
	if len(sites_finalizados) == 0:
		#Fazer o scrap de novo
		scrap(nomes, urls)

	else:
		for s in sites_finalizados:
			number = nomes.index(s) #ERRO ESQUISITO AQUI!!! :(
			nomes.remove(s)
			urls.remove(urls[number])

		scrap(nomes, urls)


#Faz o scrap de todo o sites.xml
def scrap(nome, url, categorie, execution_index):
	html = connect(url, False)
	
	if categorie:
		#Escreve o .html
		file1 = open(categorie + "-" + time.strftime("%d:%m:%Y") + "(" + execution_index + ")"  + ".html" , 'wb')
		file1.write(html)
		file1.close()

	else:
		#Escreve o .html
		file1 = open("index-" + time.strftime("%d:%m:%Y") + "(" + execution_index + ")"  + ".html" , 'wb')
		file1.write(html)
		file1.close()

	#Muda o diretorio atual	
	os.chdir("../../xml")
	if os.path.isdir("pages"):
		os.chdir("pages")
		if os.path.isdir(nome):
			print( colored.blue("  A pasta correspondende em pages/xml ja tinha sido criada antes!")) 
		else:
			os.mkdir(nome)
			print(colored.green("  A pasta correspondente em pages/xml acabou de ser criada!"))
	else:
		os.mkdir("pages")
		os.chdir("pages")
		if os.path.isdir(nome):
			print( colored.blue("  A pasta correspondende em pages/xml ja tinha sido criada antes!")) 
		else:
			os.mkdir(nome)
			print(colored.green("  A pasta correspondente em pages/xml acabou de ser criada!"))
	
	os.chdir("../../pages/" + nome)
	

"""
if __name__ == "__main__":	

	#Parse do arquivo ../../public/xml/sites.xml
	#BAIXA O 'TEST SET'
	DOMTree = xml.dom.minidom.parse("../../public/xml/sites.xml")
	sites = DOMTree.documentElement
	site = sites.getElementsByTagName("site")
	for s in site:
		n = s.getElementsByTagName("nome")[0]
		u = s.getElementsByTagName("url")[0]
		nome = n.childNodes[0].data
		url = u.childNodes[0].data

		try:
			#BAIXA O 'TEST SET'
			scrap()

		except:
			
			->Colocar algo aqui quando houver alguma falha em baixar o site
			->Se der errado, simplesmente pular o site e ir para o proximo
			
        	#Tentar mais uma vez com outro proxy antes de mostrar a mensagem
        	#apocalipse.partial_clear(True, execution_index)


    #BAIXA O 'TRAIN SET' -> JA CATEGORIZADO
	DOMTree2 = xml.dom.minidom.parse("../../public/xml/sites_by_categorie.xml")
	sites2 = DOMTree2.documentElement
	categorie = sites2.getElementsByTagName("categoria")
	for c in categorie:
		nomes2 = []
		urls2 = []
		site2 = c.getElementsByTagName("site")

		#print("CATEGORIA:" + colored.red(c.attributes["nome"].value))
		for s in site2:
			n2 = s.getElementsByTagName("nome")[0]
			u2 = s.getElementsByTagName("url")[0]
			nome2 = n2.childNodes[0].data
			url2 = u2.childNodes[0].data
			scrap(nome2, url2, c.attributes["nome"].value)

"""


"""
	global_index = 0
	site_atual = ""
	files_f = []
	files_matched = []
	files_f = glob.glob("*.html")
	if files_f:
		for f in files_f:
			#Ha um arquivo com a data (-1 = nao existe)
			if f.find(time.strftime("%d:%m:%Y")) != -1 :
				#Lista de arquivos que possuem a data de hj
				files_matched.append(f)
				number = len(files_matched)
				index = number + 1 
				global_index = index
				html = connect(url, False)

				if categorie:
					#Escreve o .html
					file1 = open(categorie + "-" + time.strftime("%d:%m:%Y") + "(" + str(index) + ")"  + ".html" , 'wb')
					file1.write(html)
					file1.close()

				else:
					#Escreve o .html
					file1 = open("index-" + time.strftime("%d:%m:%Y") + "(" + str(index) + ")"  + ".html" , 'wb')
					file1.write(html)
					file1.close()
					
	else:
		 
		global_index = 1   
		html = connect(url, False)
		#Escreve o .html
				      
		if categorie:
			file1 = open(categorie + "-" + time.strftime("%d:%m:%Y") + "(1).html" , 'wb')
			file1.write(html)
			file1.close()

		else:
			file1 = open("index-" + time.strftime("%d:%m:%Y") + "(1).html" , 'wb')
			file1.write(html)
			file1.close()	
	"""