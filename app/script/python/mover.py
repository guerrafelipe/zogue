# -*- encoding: utf-8 -*-
from lxml import html
import requests
import os, sys
import glob
import shutil
import sched, time
from datetime import datetime
from clint.textui import colored 
import re
import xml.dom.minidom
import xml.etree.ElementTree as ET 
import urllib2
import socket
from xml.dom.minidom import parse, parseString

#only use bs4
from bs4 import BeautifulSoup as bs

#Set the encoding
reload(sys)
sys.setdefaultencoding('UTF8')

"""
=======================================================================================================
=																									  =
=												BEGIN												  =		
=																									  =
========================================================================================================
"""
def move(onlyToday = True, execution_index = 1):
	print(colored.green("SCRIPT ATUAL: MOVER"))
	#Set correct path
	path = "../../public/pages"
	os.chdir(path)
	folders_list = []
	folders_list = glob.glob("*")

	os.chdir("../../script/php")
	for n in folders_list:	
		if onlyToday == True:
			files_list = glob.glob(n + "*" + time.strftime("%d:%m:%Y") + "(" + str(execution_index) + ")" + ".xml")

		else:
			files_list = glob.glob(n + "*.xml")

		for f in files_list:
			#Mover para ../../public/xml/pages/nome
			try:
				shutil.move(f, "../../public/xml/pages/" + n + "/")
				print("->Arquivo Movido:" + colored.green(f))
				print("  DE:" + colored.blue("../php"))
				print("  PARA:" + colored.blue("../../public/xml/pages/" + n + "/"))
			except:
				continue
	os.chdir("../python")

if __name__ == "__main__":
	move(False)