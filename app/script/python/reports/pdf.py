# -*- encoding: utf-8 -*-
import fpdf as pyfpdf
from fpdf import FPDF, HTMLMixin
import mysql.connector
from clint.textui import colored
import os, sys
import time
from nltk.tokenize import word_tokenize

#MUDA O ENCODING
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')

cnx = mysql.connector.connect(user='root', database='zogue_site_development')
cursor = cnx.cursor( dictionary = True, buffered=True)           
cursor.execute("SELECT Title, suggested_category FROM human_interactions;")       
data = cursor.fetchall()

html = """
<H1 align="center">
    Zogue
</H1>

<h2>Lista de sites por categoria</h2>
<p>
Segue a lista de todas as noticias por categoria
</p>

<table border="0" align="center" width="100%">
<thead><tr><th width="80%">Noticia</th><th width="20%">Categoria</th></tr></thead>
<tbody>
"""
for d in data:
    print("____________________________________")
    print("Titulo:" + d["Title"])
    d["Title"] = d["Title"].replace("&quot;", '"')
    d["Title"] = d["Title"].encode("utf8")  
    d["suggested_category"] = d["suggested_category"].encode("utf8")
    print("Categoria:" + d["suggested_category"])

    tokenized_title = word_tokenize(d["Title"])
    if len(tokenized_title) >= 5:
        tokenized_title.insert(5, "<tr><td>")
        tokenized_title.insert(-1, "</td></tr>")

        d["Title"] = " ".join(tokenized_title)
    
    html += """ <tr><td>%s</td><td>%s</td></tr>  """ %(d["Title"], d["suggested_category"])
    print("____________________________________")

html += """</tbody></table>"""


cursor.close()
cnx.close()

class MyFPDF(FPDF, HTMLMixin):
    pass

pdf = MyFPDF()
#First page
pdf.add_page()
pdf.add_font('DejaVu', '', 'fonts/DejaVuSansMono.ttf', uni=True)
pdf.write_html(html)
pdf.output('html.pdf', 'F')
