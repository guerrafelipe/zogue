require 'test_helper'

class PlataformControllerTest < ActionController::TestCase
  test "should get about" do
    get :about
    assert_response :success
  end

  test "should get team" do
    get :team
    assert_response :success
  end

  test "should get jobs" do
    get :jobs
    assert_response :success
  end

  test "should get contact" do
    get :contact
    assert_response :success
  end

  test "should get ana" do
    get :ana
    assert_response :success
  end

end
