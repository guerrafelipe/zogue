require 'test_helper'

class UserMailerControllerTest < ActionController::TestCase
  test "should get alert" do
    get :alert
    assert_response :success
  end

  test "should get confirm" do
    get :confirm
    assert_response :success
  end

end
