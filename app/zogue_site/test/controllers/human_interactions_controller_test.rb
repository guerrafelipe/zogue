require 'test_helper'

class HumanInteractionsControllerTest < ActionController::TestCase
  setup do
    @human_interaction = human_interactions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:human_interactions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create human_interaction" do
    assert_difference('HumanInteraction.count') do
      post :create, human_interaction: { Title: @human_interaction.Title, suggested_category: @human_interaction.suggested_category, user_id: @human_interaction.user_id }
    end

    assert_redirected_to human_interaction_path(assigns(:human_interaction))
  end

  test "should show human_interaction" do
    get :show, id: @human_interaction
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @human_interaction
    assert_response :success
  end

  test "should update human_interaction" do
    patch :update, id: @human_interaction, human_interaction: { Title: @human_interaction.Title, suggested_category: @human_interaction.suggested_category, user_id: @human_interaction.user_id }
    assert_redirected_to human_interaction_path(assigns(:human_interaction))
  end

  test "should destroy human_interaction" do
    assert_difference('HumanInteraction.count', -1) do
      delete :destroy, id: @human_interaction
    end

    assert_redirected_to human_interactions_path
  end
end
