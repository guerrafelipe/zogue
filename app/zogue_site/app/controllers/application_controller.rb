class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?


  require Rails.root.join("lib/custom_public_exceptions")
  config.exceptions_app = CustomPublicExceptions.new(Rails.public_path)
  config.encoding = "utf-8" 
  
  """
  protected
  def configure_permitted_parameters
     devise_parameter_sanitizer.permit(:user, keys: [:name, :email, :password, :password_confirmation, :about, :avatar, :username])
    devise_parameter_sanitizer.for(:user) << :avatar
    devise_parameter_sanitizer.for(:user) << :about
    devise_parameter_sanitizer.for(:user) << :username
  end
  """
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:name, :email) }
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :username, :email, :password, :password_confirmation, :avatar, :about) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :username, :email, :password, :password_confirmation, :avatar, :about, :current_password) }
  end
  
  """
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :avatar])
    devise_parameter_sanitizer.permit(:account_update, keys: [:username, :avatar])
  end
  """

end
