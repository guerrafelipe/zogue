class SaveController < ApplicationController

	#HTTP POST
 	def create
 	  clip_name =  params[:save][:clipName]
      new_id = params[:save][:news_id]
 	  @new = News.find(new_id)
 	  @clip = Clip.where(:name => clip_name, :user_id => current_user.id)
 	  clip = Clip.find(@clip.pluck(:id)[0])
 	  clip.news << @new
 	  flash[:notice] = 'Successfuly saved the new to "' + clip_name + '"'
	  redirect_to news_index_path 	  

 	end	

 	#HTTP POST
 	def destroy	
	  new_id = params[:new_id]
 	  clip_id = params[:clip_id]
 	  clip = Clip.find(clip_id)
 	  clip.news.delete(new_id)
 	  flash[:notice] = "New successfuly removed from clip"
 	  redirect_to clips_index_path
 	end

end
