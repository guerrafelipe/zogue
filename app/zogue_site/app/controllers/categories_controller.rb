class CategoriesController < ApplicationController
  before_action :authorize, only: [:index, :new, :edit, :create, :update, :destroy]
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
   
  # GET /categories
  # GET /categories.json
  def index
    @categories = Category.all
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    if signed_in?
      @branches = Branch.all
      @user_name = current_user.name
      @user_id = current_user.id
      @branch = Branch.new
    end

    @human_interaction = HumanInteraction.new
    
    #@filtered_categories[0].news

    @news = News.all.order(:cached_votes_up => :desc)
    @last_update = Time.now.strftime('%H:%M')
    @categories_name = Category.pluck(:Name)

    authorize! :read, @news

    @category_id =  params[:id]
    @category = Category.find(@category_id)
    @category_name = Category.where(id: @category_id).pluck(:Name)[0]
    @uncategorized = @category.news.where(subcategory_id: 0)

    #@pics_id = category.pictures.pluck(:id) #pics_id of a specific category 
    #@images = Dir.glob("#{Rails.root}/public/system/pictures/images/000/000/#{@category_id}/*.jpg" )
  end

  def showimages
    @category_images = Picture.where(category_id: params[:id])
    @category = Category.find(params[:id])
    @picture = Picture.new
  end

  # GET /categories/new
  def new
    @category = Category.new
  end

  # GET /categories/1/edit
  def edit
  end

  # POST /categories
  # POST /categories.json
  def create
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
        if params[:images]
          params[:images].each { |image|
            @category.pictures.create(image: image)
          }
        end

        format.html { redirect_to @category, notice: 'Category was successfully created.' }
        format.json { render :show, status: :created, location: @category }
      else
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    respond_to do |format|
      if @category.update(category_params)
         if params[:images]
          params[:images].each { |image|
            @category.pictures.create(image: image)
          }
        end

        format.html { redirect_to @category, notice: 'Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @category }
      else
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @category.destroy
    respond_to do |format|
      format.html { redirect_to categories_url, notice: 'Category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:Name, :Description)
    end

    def authorize
      redirect_to(root_path) unless user_signed_in? and current_user.is_admin?
    end

end
