#encoding: utf-8
require 'fileutils'
class NewsController < ApplicationController
  before_action :authorize, only: [:show, :edit, :update, :destroy, :new, :create, :pictures, :uploadPictures]
  before_action :set_news, only: [:show, :edit, :update, :destroy, :upvote, :downvote]
  before_filter :authenticate_user!,
    :only => [:destroy, :new, :edit, :create, :update]
 
  # GET /news
  # GET /news.json
  def index
    #Ok
    news = News.where(news_edition: DateTime.now.to_date )
    #for n in news
    #  if n.already_known == false
    #    image_name = n.image_file_name
    #    new_category = image_name.split("_")[0]
    #    new_id = n.id
    #    folders = ["headline", "medium", "original", "thumb"]
    #    filename = "#{Rails.root}/app/assets/images/news_image/#{new_category}/#{image_name}"
    #    for f in folders
    #      dest_folder = "#{Rails.root}/public/system/news/images/000/000/#{new_id}/#{f}/"
    #      FileUtils.mkdir_p(dest_folder)
    #      FileUtils.cp(filename, dest_folder)
    #    end
    #    News.update(new_id, :already_known => true)      
    #  end
    #end
    
    """
    OPCOES PARA A ESCOLHA DA IMAGE:
    1)Scrap da noticia (Python manda a url da imagem e o nokogiri baixa)
    2)Banco de dados de imagens escolhida por descricao da subcategoria
    3)Imagens do Wikipedia (baseada em reconhecimento de entidades- NER)
    4)Imagem escolhida randonicamentes
    """
    for n in news
        if n.already_known == false
          @new = News.find(n.id)
          new_category = News.where(id: n.id).pluck(:category_id)[0]
          category = Category.find(new_category)
          pictures = category.pictures
          random_picture = pictures.order("RAND()").first
          News.update(@new, :already_known => true, :image_file_name => random_picture.image_file_name)      
        end
    end
    
    @news = News.order(:cached_votes_up => :desc)    
    if signed_in?
      @branches = Branch.all
      @user_name = current_user.name
      @user_id = current_user.id
      @branch = Branch.new
      @user = User.find(@user_id)
      @clips = @user.clips.pluck(:name)
    end

    @human_interaction = HumanInteraction.new
    categories = Category.all
    @filtered_categories = [] #Categories that have at least one new of the day
    for c in categories
      news_categorie = c.news.where(news_edition: DateTime.now.to_date )
      if news_categorie.count > 0
            @filtered_categories << c
      end
    end
    
    @last_update = Time.now.strftime('%H:%M')
    @categories_name = Category.pluck(:Name)
    authorize! :read, @news
  end


  # GET /news/1
  # GET /news/1.json
  def show
    @category_name = Category.find(@news.category_id) 
  end

  # GET /news/new
  def new
    @news = News.new
    @categories = Category.all
    @categories_name = Category.pluck(:Name)
    @user_name = current_user.name
  end

  # GET /news/1/edit
  def edit
    @categories_name = Category.all.pluck(:Name)
    @categories = Category.all
    id = params[:id]
    category_id = News.where(id: id).pluck(:category_id)[0]
    @category_name = Category.where(id: category_id).pluck(:Name)[0]
    @images = Dir.glob("app/assets/images/news_image/#{@category_name}/*.jpg" )
  end
  
  #Acao disparada ao clicar num link
  def upvote
    @news.upvote_from current_user
    respond_to do |format|
      format.js {render "upvote"}
    end  
  end


  #downvote_from user
  def downvote
  end

  # POST /news
  # POST /news.json
  def create

    #o texto foi pego com eficiencia
    @given_category = params[:news][:category]
    @given_title = params[:news][:Title]
    @given_text = params[:news][:Text]
    @given_url = params[:news][:Url]
    @news_edition = Time.now.strftime("%Y-%m-%d")
    @created_at = Time.now
    @updated_at = Time.now
    @cat = Category.where(Name: @given_category)
    
    if !@cat.blank?
      @news = News.new(news_params.merge(:Title => @given_title, :Text => @given_text, :Url => @given_url, :news_edition => @news_edition, :category_id => @cat.pluck(:id)[0], :Author => current_user.name ))
      respond_to do |format|
        if @news.save!
          format.html { redirect_to news_index_path, notice: 'News was successfully created.' }
          format.json { render :show, status: :created, location: @news }
        else
          format.html { render :new }
          format.json { render json: @news.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT/news/1
  # PATCH/PUT/news/1.json
  def update
    @title = params[:news][:Title]
    @text = params[:news][:Text]    
    @image = params[:news][:image]
    category = params[:news][:category]
    @category_id = Category.where(Name: category).pluck(:id)[0] 
    @url = params[:news][:Url]
    @author = "Ana" 
    @id = params[:id]

    folders = ["headline", "medium", "original", "thumb"]
    filename = "#{Rails.root}/app/assets/images/news_image/#{category}/#{@image}"
    for f in folders
      dest_folder = "#{Rails.root}/public/system/news/images/000/000/#{@id}/#{f}"
      FileUtils.cp(filename, dest_folder)
    end

    respond_to do |format|
      if News.update(@id, :Title => @title ,:image_file_name => @image, :Text => @text, :category_id => @category_id, :Url => @url)
        format.html { redirect_to news_index_path, notice: 'News was successfully updated.' }     
      else
        format.html { redirect_to news_index_path, notice: 'News was successfully updated.' }
      end
    end

   # respond_to do |format|
   #   if @news.update(news_params.merge(:Title => @title, :image_file_name => @image, :Text => @text, :Url => @url, :news_edition => @news_edition, :category_id => @category_id, :Author => @author))
   #     format.html { redirect_to @news, notice: 'News was successfully updated.' }
   #     format.json { render :show, status: :ok, location: @news }
   #   else
   #     format.html { render :edit }
   #     format.json { render json: @news.errors, status: :unprocessable_entity }
   #   end
   # end

  end

  # DELETE /news/1
  # DELETE /news/1.json
  def destroy
    @news.destroy
    respond_to do |format|
      format.html { redirect_to news_index_url, notice: 'News was successfully destroyed.' }
      format.json { head :no_content }
      format.js
    end
  end

  #def changecategory
  #  #Aumenta o training model em app/public
  #  respond_to do |format|
  #    format.html {redirect_to news_index_url, notice: 'Category changed.'}
  #    format.js
  #  end
  #end

  def pictures
    @categories_name = Category.all.pluck(:Name)
  end

  def uploadPictures

    category =  params[:new][:category]
    path = "#{Rails.root}/app/assets/images/news_image/#{category}"  
    #imagem de 0 a 9 -> count = 0
    count = Dir.glob(path + "/*").count
    file_name = category + "_" + count.to_s + ".jpg"
    tmp = params[:upload][:file].tempfile
    destiny_file = path + "/" + file_name
    FileUtils.move tmp.path, destiny_file  
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_news
      @news = News.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def news_params


       """
       Title, Text, Author, Url, created_at, updated_at, category_id, news_edition
       """
      params.require(:news).permit(:Title, :Text, :category_id, :created_at, :updated_at, :Author, :Url, :news_edition, :bootsy_image_gallery_id)
      params.require(:news).permit(:image_file_name, :already_known)
    end

    def authorize
       redirect_to(root_path) unless user_signed_in? and current_user.is_admin?
    end

end
