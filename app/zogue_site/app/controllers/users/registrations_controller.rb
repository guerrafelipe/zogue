class Users::RegistrationsController < Devise::RegistrationsController
# before_action :configure_sign_up_params, only: [:create]
# before_action :configure_account_update_params, only: [:update]
prepend_before_action :check_captcha, only: [:create] # Change this to be any actions you want to protect.
  
  #Nao funciona
  def create
    @user = User.create(sign_up_params)
    if @user.save
      render :html => "OK"
    else
      render :html => "OPS."
    end
  end

  # GET /resource/sign_up
  # def new
  #   super
  # end

  # POST /resource
  # def create
  #    # app/controllers/users_controller.rb
  #    @user = User.new(params[:user].permit(:name, :username))
  #    if verify_recaptcha(model: @user) && @user.save
  #      redirect_to @user
  #    else
  #      respond_to do |format|
  #        flash["error"] = "You are probably a robot"
  #      end
  #    end  
  # end

 # def create
 # end

  # GET /resource/edit
   #def edit
   #  super
   #  
   #end

  # PUT /resource
   #def update
   #  render :html => params[:about]
   #end

  # DELETE /resource
   #def destroy
   #  super
   #end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

 #  private

  def sign_up_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

 # def account_update_params
 #   params.require(:user).permit(:name, :username, :email, :password, :password_confirmation, :avatar, :about)
 # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
  private
  def check_captcha
    if verify_recaptcha
      true
    else
      self.resource = resource_class.new sign_up_params
      respond_with_navigational(resource) { render :new }
    end 
  end

end
