# app/controllers/my_devise/registrations_controller.rb
class MyDevise::RegistrationsController < Devise::RegistrationsController
prepend_before_action :check_captcha, only: [:create]

  def new
  	super
  end
  
  def create
    super   
  end


  private
  def check_captcha
    if verify_recaptcha
      true
    else
      self.resource = resource_class.new sign_up_params
      respond_with_navigational(resource) { render :new }
    end 
  end

  protected
  def after_sign_up_path_for(resource)
    '/profile/createprofile' # Or :prefix_to_your_route
  end
  
end 