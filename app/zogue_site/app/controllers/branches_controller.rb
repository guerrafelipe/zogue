class BranchesController < ApplicationController
  before_action :authorize, only: [:index]
  before_action :set_branch, only: [:show, :edit, :update, :destroy, :upvote, :downvote]

  # GET /branches
  # GET /branches.json
  def index
    @branches = Branch.all    
  end

  # GET /branches/1
  # GET /branches/1.json
  def show
  end

  # GET /branches/new
  def new
    @branch = Branch.new
  end

  # GET /branches/1/edit
  def edit
  end

  # POST /branches
  # POST /branches.json
  def create
    @branch = Branch.new(branch_params)
    @new_id = params[:branch][:news_id]

    respond_to do |format|
      if @branch.save
        format.html { redirect_to @branch, notice: 'Branch was successfully updated.'}
        format.json { render :show, status: :created, location: @branch }
      else
        format.html { render :new }
        format.json { render json: @branch.errors, status: :unprocessable_entity }
      end
    end
  end

  def upvote

    """
    ->Diferentes usuarios devem ter peso diferente. O autor da versao
    da noticia ao votar deve ter menos peso que os outros (razao obvia)
    """
       
    @branch.upvote_from current_user

    respond_to do |format|
      format.html { redirect_to news_index_path }
      format.js
    end

  end

  def downvote
    @branch.downvote_from current_user
    respond_to do |format|
      format.html { redirect_to news_index_path }
      format.js
    end
  end

  # PATCH/PUT /branches/1
  # PATCH/PUT /branches/1.json
  def update
    respond_to do |format|
      if @branch.update(branch_params)
        format.html { redirect_to @branch, notice: 'Branch was successfully updated.' }
        format.json { render :show, status: :ok, location: @branch }
      else
        format.html { render :edit }
        format.json { render json: @branch.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /branches/1
  # DELETE /branches/1.json
  def destroy
    @branch.destroy
    respond_to do |format|
      format.html { redirect_to news_index_path, notice: 'Branch was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_branch
      @branch = Branch.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def branch_params
      params.require(:branch).permit(:Title, :author, :news_id, :Text, :new_id, :reaction)
    end

    def authorize
      redirect_to(root_path) unless user_signed_in? and current_user.is_admin?
    end

end
