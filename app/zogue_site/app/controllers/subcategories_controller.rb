class SubcategoriesController < ApplicationController
  before_action :authorize, only: [:index, :show, :new, :create, :edit, :update, :destroy]
  before_action :set_subcategory, only: [:show, :edit, :update, :destroy]

  # GET /subcategories
  # GET /subcategories.json
  def index
    @subcategories = Subcategory.all
    @categories = Category.all
  end

  # GET /subcategories/1
  # GET /subcategories/1.json
  def show
  end

  # GET /subcategories/new
  def new
    @categories_name = Category.all.pluck(:Name)
    @subcategory = Subcategory.new
  end

  # GET /subcategories/1/edit
  def edit
    @categories_name = Category.all.pluck(:Name)
    subcategorie = Subcategory.where(id: params[:id])
    @description = subcategorie.pluck(:Description)[0]
  end

  # POST /subcategories
  # POST /subcategories.json
  def create

    categoryName =  params[:subcategory][:category_id]
    category = Category.where(Name: categoryName)
    
    if !category.blank?
      @category_id = category.pluck(:id)[0]
      @subcategory = Subcategory.new(subcategory_params.merge(:Name => params[:subcategory][:Name], :Description=>params[:subcategory][:Description], :category_id =>@category_id ))
      respond_to do |format|
        if @subcategory.save
          format.html { redirect_to subcategories_index_path, notice: 'Subcategory was successfully created.' }
          format.json { render :show, status: :created, location: @subcategory }
        else
          format.html { render :new }
          format.json { render json: @subcategory.errors, status: :unprocessable_entity }
        end
      end

    end
  end

  # PATCH/PUT /subcategories/1
  # PATCH/PUT /subcategories/1.json
  def update
    categoryName =  params[:subcategory][:category_id]
    category = Category.where(Name: categoryName)

    if !category.blank?
      @category_id = category.pluck(:id)[0]
      respond_to do |format|
        if @subcategory.update(subcategory_params.merge(:Name => params[:subcategory][:Name], :Description=>params[:subcategory][:Description], :category_id =>@category_id ))
          format.html { redirect_to subcategories_index_path, notice: 'Subcategory was successfully updated.' }
          format.json { render :show, status: :ok, location: @subcategory }
        else
          format.html { render :edit }
          format.json { render json: @subcategory.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /subcategories/1
  # DELETE /subcategories/1.json
  def destroy
    @subcategory.destroy
    respond_to do |format|
      format.html { redirect_to subcategories_url, notice: 'Subcategory was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subcategory
      @subcategory = Subcategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subcategory_params
      params.require(:subcategory).permit(:Name, :Description, :category_id)
    end

    def authorize
       redirect_to(root_path) unless user_signed_in? and current_user.is_admin?
    end
end
