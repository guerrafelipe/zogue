class UserMailerController < ApplicationController
   before_action :authorize
  
  def alert
  end

  def confirm
  end

  private
  def authorize
    redirect_to(root_path) unless user_signed_in? and current_user.is_admin?
  end

end
