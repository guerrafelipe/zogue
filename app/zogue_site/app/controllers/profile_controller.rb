class ProfileController < ApplicationController
  before_filter :authenticate_user!,
  
 def index
 	@user_id = params[:id]
 	@user = User.find( @user_id )
 	user = User.where(id: @user_id)
 	@user_interests = @user.interests.pluck(:Name)
 	@user_name = user.pluck(:Name)[0]
 	@user_about = user.pluck(:about)[0]
 	@status = user.pluck(:is_admin)[0]
 	@created_at = user.pluck(:created_at)[0]

 	@list_interests = []
 	for u in @user_interests
 		if not @list_interests.include?(u)
 			@list_interests << u
 		end
 	end

 end

 #HTTP GET
 def createprofile
 	if !user_signed_in?
 		"error/permission"
 	else
 		@categories_names = Category.all.pluck(:Name)
 	end
 end

 #HTTP POST
 def create
 	interests = params[:createprofile][:interests]
 	@user = User.find(current_user.id)
 	@user.update_attribute(:avatar, params[:createprofile][:avatar])
	@user.update_attribute(:about, params[:createprofile][:about])
	
	for i in interests
		if !i.blank?
			category = Category.where(Name: i)
			@user.interests << category		
		end
	end
	redirect_to "/profile/#{current_user.id}"
 end

end
