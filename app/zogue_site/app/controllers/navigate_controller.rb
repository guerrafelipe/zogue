class NavigateController < ApplicationController
  before_action :authorize, only: [:index]
  def index
  	id = params[:id]
  	@new = News.find(id)
  end

  def authorize
    redirect_to(root_path) unless user_signed_in? and current_user.is_admin?
  end

end
