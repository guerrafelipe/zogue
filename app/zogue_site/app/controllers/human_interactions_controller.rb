class HumanInteractionsController < ApplicationController
  before_action :authorize
  before_action :set_human_interaction, only: [:show, :edit, :update, :destroy]

  # GET /human_interactions
  # GET /human_interactions.json
  def index
    @human_interactions = HumanInteraction.all
  end

  # GET /human_interactions/1
  # GET /human_interactions/1.json
  def show
  end

  # GET /human_interactions/new
  def new
    @human_interaction = HumanInteraction.new
  end

  # GET /human_interactions/1/edit
  def edit
  end

  # POST /human_interactions
  # POST /human_interactions.json
  def create
    typedCategory = params[:human_interaction][:suggested_category]
    categoryRow = Category.where(Name: typedCategory)
    if categoryRow
      categoryID = categoryRow.pluck(:id)[0]
      newID =  params[:human_interaction][:new_id]
      @newRow = News.where(id: newID)
      newRow = News.find(@newRow.pluck(:id)[0])
      add_entities = params[:human_interaction][:add_entities]
      if add_entities == "Yes"
        validate_entities = true  
      else
        validate_entities = false
      end
      previousCategory_id = @newRow.pluck(:category_id)[0]
      previousCategory = Category.where(id: previousCategory_id).pluck(:Name)[0]

      if typedCategory != previousCategory      
        category = Category.find(categoryRow.pluck(:id)[0])
        pictures = category.pictures
        random_picture = pictures.order("RAND()").first
        @newRow.update(newID, :category_id => categoryID, :image_file_name => random_picture.image_file_name, :subcategory_id => 0)
        review = true
      else
        review = false
      end
      @pass_id = categoryID
      @human_interaction = HumanInteraction.new(:Title => newRow.Title, :suggested_category => typedCategory, :user_id => current_user.id, :new_id => newID, :add_entities => validate_entities, :review => review)
      respond_to do |format|
        if @human_interaction.save
          format.html { redirect_to @human_interaction, notice: 'Human interaction was successfully created' }
          format.json { render :show, status: :created, location: @human_interaction }
          format.js
        else
          format.html { render :new }
          format.json { render json: @human_interaction.errors, status: :unprocessable_entity }
          format.js
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to news_index_path, notice: "Wrong category, try again!" }
      end
    end
  end

  # PATCH/PUT /human_interactions/1
  # PATCH/PUT /human_interactions/1.json
  def update
    respond_to do |format|
      if @human_interaction.update(human_interaction_params)
        format.html { redirect_to @human_interaction, notice: 'Human interaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @human_interaction }
      else
        format.html { render :edit }
        format.json { render json: @human_interaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /human_interactions/1
  # DELETE /human_interactions/1.json
  def destroy
    @human_interaction.destroy
    respond_to do |format|
      format.html { redirect_to human_interactions_url, notice: 'Human interaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_human_interaction
      @human_interaction = HumanInteraction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def human_interaction_params
      params.require(:human_interaction).permit(:Title, :add_entities, :new_id, :suggested_category, :user_id)
    end

    def authorize
      redirect_to(root_path) unless user_signed_in? and current_user.is_admin?
    end
end
