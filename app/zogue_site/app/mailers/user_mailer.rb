class UserMailer < ActionMailer::Base
	
	default :from => "noreply@zogue.com.br"
	def registration_confirmation(user)
		@user = user
		mail(:to => "#{user.name} <#{user.email}", :subject => "Please confirm your registration")
	end

end	