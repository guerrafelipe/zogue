json.array!(@sites) do |site|
  json.extract! site, :id, :Name, :Url, :Country, :State, :Model, :category_id, :last_scrapped, :score
  json.url site_url(site, format: :json)
end
