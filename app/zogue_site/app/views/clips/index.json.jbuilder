json.array!(@clips) do |clip|
  json.extract! clip, :id, :name, :description, :user_id
  json.url clip_url(clip, format: :json)
end
