json.array!(@news) do |news|
  json.extract! news, :id, :Title, :Text, :category, :Author, :Url
  json.url news_url(news, format: :json)
end
