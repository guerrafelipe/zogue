json.array!(@statistics) do |statistic|
  json.extract! statistic, :id, :listed_sites, :genesis, :runner, :primary_classification, :news_number, :rightansw_number, :global
  json.url statistic_url(statistic, format: :json)
end
