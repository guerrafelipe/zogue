json.array!(@human_interactions) do |human_interaction|
  json.extract! human_interaction, :id, :Title, :suggested_category, :user_id
  json.url human_interaction_url(human_interaction, format: :json)
end
