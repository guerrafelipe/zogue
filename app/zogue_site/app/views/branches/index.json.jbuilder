json.array!(@branches) do |branch|
  json.extract! branch, :id, :Title, :Text, :new_id, :reaction
  json.url branch_url(branch, format: :json)
end
