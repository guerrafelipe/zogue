module NavigateHelper
	def strip_url(target_url)
  			target_url.gsub("http://", "")
            	.gsub("https://", "")
            	.gsub("www.", "")
	end

	def do_iframe(url)
		content_tag(:iframe, nil, src: "//www." + strip_url(url) , :class=>"col-md-12", :style=>"height:500px;")
	end

end
