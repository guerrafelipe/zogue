module BranchesHelper

	def get_title(branch_id)
		new_id = Branch.where(id: branch_id).pluck(:new_id)[0]
		return News.where(id: new_id).pluck(:Title)[0]
	end

	def get_user(user_name)
		user = User.find_by(Name: user_name)
		return user
	end

	def get_text(text, id)
		txt = text.split(" ")
        if txt.count  >= 20
          small_text = txt[0..20]
          small_text = small_text.join(" ")
		  raw(small_text + ' <a href="/branches/' + id.to_s + '"">Continue reading</a>')
		
		else
			return text
		end
	end
end
