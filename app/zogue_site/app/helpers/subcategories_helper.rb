module SubcategoriesHelper

	def get_name(category_id)
		return Category.where(id: category_id).pluck(:Name)[0]
	end

	"""
	def get_text(description, q)
		
		Euclidian Division p | q
						   r |___
						   	   s
		p = qs + r -> s = (p-r) / q
		p: numero de palavras
		q: numero de palavras por linha
		s: numero de linhas
		r: numero de palavras que sobram apos a divisao em linhas
		
		description = description.split(";")

		if description.count >= q
			p = description.count
			r = description.count % q
			
			for x in (0..r)

				for a in (1..s)
					raw(description[0..q])
				end
			end

		else
			return ';'.join(description)
		end
	end
	"""

	"""
	def get_text(description, q)
		count = 0
		str = ""
		size = description.split(";").count
		while count < size
			i = count + q
			str = description[count..i] + <br/>
			count += q
		end
		raw(str)
	end
	"""
end
