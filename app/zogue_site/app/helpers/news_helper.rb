module NewsHelper
	def filtertodayNews(all_news)
		news =  all_news.where(news_edition: DateTime.now.to_date)
		count_news = news.count
		
		if count_news > 6
			return news [0..5]
		else
			return news
		end
	end

	def filtertodayNews2(all_news)
		news =  all_news.where(news_edition: DateTime.now.to_date)
		return news
	end

    def count_spare(category_id)
    	news = News.where(news_edition: DateTime.now.to_date).where(category_id: category_id)
    	news_spare = news.count - 6

    	if news_spare > 1
    		return news_spare.to_s + " more news to read than those displayed" 
    	elsif news_spare == 1
    		return news_spare.to_s + " more new to read than those displayed"
    	else
    		return "No more news to read yet, than those displayed"
    	end
    end

    def return_category_name(new_id)
    	@new = News.where(id: new_id).pluck(:category_id)[0]
    	@category = Category.where(id: @new).pluck(:Name)[0]
    	return @category
    end

    def select_image(new_id)
    	new_category = News.where(id: new_id).pluck(:category_id)[0]
    	new_image_file_name = News.where(id: new_id).pluck(:image_file_name)[0]
    	category = Category.find(new_category)
    	pictures = category.pictures
    	pic1 = pictures.where(image_file_name: new_image_file_name).pluck(:id)[0]
    	return Picture.find(pic1)
    end

    def orderCategories(categories)
 		#Order by the number of 'child' news. Categories with more news goes to the top
    	dic = {}
    	for c in categories
    		dic[c] = c.news.count
    	end
    	return Hash[dic.sort_by {|k,v| v}.reverse].keys
    end

    def get_subcategoryname(new_id)
    	n = News.where(id: new_id)
    	subcategory_id = n.pluck(:subcategory_id)[0]
    	return Subcategory.where(id: subcategory_id).pluck(:Name)[0]
    end

    def get_info(entity, entity_type, new_category)
    	url = "https://pt.wikipedia.org/wiki/" + entity
    	return raw('<a href="' + url +'">' + entity + '</a>')
    end

	def summarizeText(text, url)
		if text.mb_chars.length > 200
			summarized_text = text[0..200]
			content_tag(:a, summarized_text +"...", :class => "new-text", :href=> url) 
		else
			content_tag(:a, text, :class => "new-text", :href=> url) 
		end
	end

	def alreadyValidated(id)
		interaction = HumanInteraction.where(new_id: id)
		if interaction.count > 0
			return true
		else
			return false
		end
	end

	
	def headline(all_news)
		#Order all_news by the number of clicks
		
		todday_allNews = all_news.where(news_edition: DateTime.now.to_date)
		ranked_news = todday_allNews.order(:cached_votes_up => :desc)
		return ranked_news[0]

		"""
		dic = {}
		for c in all_news
    		dic[c] = c.clicks_count
    	end
    	
    	ordered_news = Hash[dic.sort_by {|k,v| v}.reverse].keys
    	
    	#returns an object
    	return ordered_news[0]
    	"""
	end
	
	def getAuthorPicture(author)
		@user = User.where(Name: author)
		return @user
	end

	def getAuthorProfile(author)
		@user = User.where(Name: author)
		@user_id = @user.pluck(:id)[0]
		return "profile/#{@user_id}"
	end

	def countBranches(new)
		all_branches = Branch.where(news_id: new.id)
		return all_branches.count
	end

	def getCatName(new_id)
		category_id = News.where(id: new_id).pluck(:category_id)[0]
		return Category.where(id: category_id).pluck(:Name)[0]
	end
end
