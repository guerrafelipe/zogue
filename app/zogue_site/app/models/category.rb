class Category < ActiveRecord::Base
 has_many :news,:class_name => "News"
 has_many :pictures, :dependent => :destroy
 has_many :subcategories
 validates :Name, presence: true
 validates :Description, presence: true
 
 has_and_belongs_to_many :users

 accepts_nested_attributes_for :pictures, :reject_if => lambda { |t| t['picture'].nil? }
end
