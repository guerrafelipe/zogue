class Picture < ActiveRecord::Base
	belongs_to :category
	has_attached_file :image, styles: { medium: "300x300>", headline: "960x540>" , thumb: "200x200>" }, default_url: "/images/:style/missing.png"	
	do_not_validate_attachment_file_type :image
	validates :category_id, presence: true
end
