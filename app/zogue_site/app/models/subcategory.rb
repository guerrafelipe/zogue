class Subcategory < ActiveRecord::Base
	belongs_to :category
	has_many :news,:class_name => "News"
	has_many :pictures
end
