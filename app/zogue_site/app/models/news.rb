class News < ActiveRecord::Base 
 include Bootsy::Container


 has_attached_file :image,
    :path => "public/system/news/images/000/000/:id/:filename",
    :url => "public/system/news/images/000/000/:id/:basename.:extension",
    styles: { medium: "300x300>", headline: "960x540>" , thumb: "200x200>" },
    default_url: "/images/:style/missing.png"
 
 validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
 acts_as_votable

  #belongs_to is a tip of where to put the foreign key
  belongs_to :category
  belongs_to :subcategory
  has_many :branches
  has_and_belongs_to_many :clips
  
  Paperclip.interpolates :img_name do |attachment, style|
    attachment.instance.img_name
  end

  #validates :Title, presence: true
  #validates :Text, presence: true
  #validates :Text, length: {minimum:100, maximum:1000}, :on => :create
  #validates :category_id, presence: true
  #validates :Url, presence: true
  #validates :Author, presence: true
end
