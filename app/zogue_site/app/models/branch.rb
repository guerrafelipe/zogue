class Branch < ActiveRecord::Base
  belongs_to :news
  validates_presence_of :Title
  validates_presence_of :Text
  validates_presence_of :reaction
  acts_as_votable
end
