require'digest/md5'

class User < ActiveRecord::Base
  before_create :confirmation_token
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
 devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
 has_attached_file :avatar, styles: { medium: "300x300>", thumb: "80x80>" }, :default_url => ActionController::Base.helpers.asset_path("default_user.png") 
 validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
 acts_as_voter

 validates :name, presence: true
 validates_uniqueness_of :email, :on => :create, :message => "must be unique"
 validates :email, :email_format => true
 
 has_and_belongs_to_many :interests, :class_name => "Category"
 has_many :clips
 
 private
 def confirmation_token
 	if self.confirm_token.blank?
 		self.confirm_token = SecureRandom.urlsafe_base64.to_s
 	end
 end


end


