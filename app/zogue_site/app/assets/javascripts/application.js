// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require jquery
//= require jquery-ui
//= require jquery-migrate-min
//= require jquery_ujs
//= require bootsy
//= require turbolinks
//= require bootstrap-sprockets
//= require_tree .
//= require froala_editor.min.js
// Include other plugins.
//= require plugins/align.min.js
//= require plugins/char_counter.min.js
//= require plugins/code_beautifier.min.js
//= require plugins/code_view.min.js
//= require plugins/colors.min.js
//= require plugins/emoticons.min.js
//= require plugins/entities.min.js
//= require plugins/file.min.js
//= require plugins/font_family.min.js
//= require plugins/font_size.min.js
//= require plugins/fullscreen.min.js
//= require plugins/image.min.js
//= require plugins/image_manager.min.js
//= require plugins/inline_style.min.js
//= require plugins/line_breaker.min.js
//= require plugins/link.min.js
//= require plugins/lists.min.js
//= require plugins/paragraph_format.min.js
//= require plugins/paragraph_style.min.js
//= require plugins/quick_insert.min.js
//= require plugins/quote.min.js
//= require plugins/save.min.js
//= require plugins/table.min.js
//= require plugins/url.min.js
//= require plugins/video.min.js
//= require bootstrap-tagsinput.js
/*

   var val = $("#0").attr("class");
            var arr = val.split("-");
            var query = arr[1] + "-container";
            $("#"+query).show().siblings("div").hide();     

            
            $(".bx-pager-link").click(function(){
               var val = $(this).attr("data-slide-index");
               
               var categorie_class = $("#" + val).attr("class");
               var arr = categorie_class.split("-");
               var query = arr[1] + "-container";
               
                
               $.ajax({
                  method: "POST",
                  url: "/new",
                  data: arr[1]
               });


               //Use .fadetoggle() instead (prettier)
               $("#"+query).show().siblings("div").hide();     
            });


            */