Rails.application.routes.draw do
  
  get 'navigate/index'

  match 'auth/:provider/callback', to: 'sessions#create', via: [:get, :post]
  match 'auth/failure', to: redirect('/'), via: [:get, :post]
  match 'signout', to: 'sessions#destroy', as: 'signout', via: [:get, :post]

  resources :clips
 
  get 'user_mailer/alert'
  get 'user_mailer/confirm'

  resources :subcategories
  get 'errors/error404'

  resources :sites
  resources :statistics
  resources :human_interactions
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  
  resources :branches do
    member do
      put "like" => "branches#upvote"
      put "unlike" => "branches#downvote"
    end
  end

  match "/404" => "errors#error404", via: [ :get, :post, :patch, :delete ]
  
  get 'plataform/about'

  get 'categories/showimages/:id' => "categories#showimages"
  
  get 'plataform/team'

  get 'profile/createprofile/:id' => "profile#createprofile", :as => :createprofile_profile

  get 'plataform/jobs'
  get 'plataform/contact'

  get 'plataform/ana'
  get "subcategories/" => "subcategories#index", :as => :subcategories_index
  get 'news/pictures'
  get 'news/pictures' => 'news#pictures', :as => :news_picture
  get "/branches/:id/edit" => "branches#edit", :as => :branches_edit
  get "/branches/destroy/:id" => "branches#destroy", :as => :branches_destroy

  get "/navigate/:id" => "navigate#index"
  get "/save/:new_id/destroy/:clip_id" => "save#destroy", :as => :save_destroy

  mount Bootsy::Engine => '/bootsy', as: 'bootsy'

  #resources :users do
  #  member do
  #    get :confirm_email
  #  end
  #end

  resources :categories
  resources :news
  resources :news do
    member do
      put "create" => "news#create"
      put "like" => "news#upvote"
      put "unlike" => "news#downvote"

    end
  end

  put "uploadPictures" => "news#uploadPictures"
  put "create" => "profile#create"
  post "profile/create"
  post "save/create"
  post "news/create", :as => :create_news
  post "pictures/create", :as => :create_pictures

  get 'home/index'
  get "profile/:id" => "profile#index"
  get "/clips" => "clips#index", :as => :clips_index
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  devise_for :users, controllers: {
    registrations: "my_devise/registrations"
  }



  # You can have the root of your site routed with "root"
  root 'news#index'

  get '/human_interactions/create' => 'human_interactions#create', :as => :create_interaction
  resources :human_interactions
  
  #match '/news' => 'news#index'
  #match '/home' => 'home#index'
  #match '/users' => 'users#sign_in'
  #match '/plataform' => 'plataform#about'

  get "/users" => "users#sign_in"
  get "/home" => "home#index"
  get "/plataform" => "plataform#about"
  get "/zadpanel13" => "zadpanel13#index"

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'



  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
