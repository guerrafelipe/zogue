# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

Rails.application.config.assets.precompile += %w( bootstrap.min.css )
Rails.application.config.assets.precompile += %w( style.css )
Rails.application.config.assets.precompile += %w( token-input.css )
Rails.application.config.assets.precompile += %w( bootstrap-tagsinput.css )
Rails.application.config.assets.precompile += %w( jquery.bxslider.css )
Rails.application.config.assets.precompile += %w( desinfection.js )
Rails.application.config.assets.precompile += %w( jquery.bxslider.js )
Rails.application.config.assets.precompile += %w( rails.validations.js )
Rails.application.config.assets.precompile += %w( rails.validations.custom.js )
Rails.application.config.assets.precompile += %w( jquery.tokeninput.js )
Rails.application.config.assets.precompile += %w( bootstrap-tagsinput.js )

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w( bx_loader.gif controls.png )