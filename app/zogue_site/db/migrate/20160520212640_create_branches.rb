class CreateBranches < ActiveRecord::Migration
  def change
    create_table :branches do |t|
      t.string :Title
      t.text :Text
      t.integer :new_id
      t.string :reaction

      t.timestamps null: false
    end
  end
end
