class ChangeSubcategoryIdInNews < ActiveRecord::Migration
  def change
  	change_column :news, :subcategory_id, :integer
  end
end
