class CreateHumanClassifications < ActiveRecord::Migration
  def change
    create_table :human_classifications do |t|
      t.string :Title
      t.string :suggested_category
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
