class CreateStatistics < ActiveRecord::Migration
  def change
    create_table :statistics do |t|
      t.integer :listed_sites
      t.float :genesis
      t.float :runner
      t.float :primary_classification
      t.integer :news_number
      t.integer :rightansw_number
      t.float :global

      t.timestamps null: false
    end
  end
end
