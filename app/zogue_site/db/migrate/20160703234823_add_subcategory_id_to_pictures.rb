class AddSubcategoryIdToPictures < ActiveRecord::Migration
  def change
    add_column :pictures, :subcategory_id, :integer
  end
end
