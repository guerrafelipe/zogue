class AddAuthorToBranches < ActiveRecord::Migration
  def change
    add_column :branches, :author, :string
  end
end
