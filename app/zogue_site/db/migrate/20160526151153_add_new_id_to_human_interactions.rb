class AddNewIdToHumanInteractions < ActiveRecord::Migration
  def change
    add_column :human_interactions, :new_id, :integer
  end
end
