class AddCommentToStatistics < ActiveRecord::Migration
  def change
    add_column :statistics, :comment, :text
  end
end
