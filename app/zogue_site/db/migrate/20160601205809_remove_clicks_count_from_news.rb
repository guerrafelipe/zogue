class RemoveClicksCountFromNews < ActiveRecord::Migration
  def change
    remove_column :news, :clicks_count, :integer
  end
end
