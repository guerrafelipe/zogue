class AddReviewToHumanInteractions < ActiveRecord::Migration
  def change
    add_column :human_interactions, :review, :boolean
  end
end
