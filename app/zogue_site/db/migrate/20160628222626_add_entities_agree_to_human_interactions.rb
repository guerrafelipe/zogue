class AddEntitiesAgreeToHumanInteractions < ActiveRecord::Migration
  def change
    add_column :human_interactions, :add_entities, :boolean
  end
end
