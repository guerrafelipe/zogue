class CreateHumanInteractions < ActiveRecord::Migration
  def change
    create_table :human_interactions do |t|
      t.string :Title
      t.string :suggested_category
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
