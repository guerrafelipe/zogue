class AddAlreadyKnownToNews < ActiveRecord::Migration
  def change
    add_column :news, :already_known, :boolean
  end
end
