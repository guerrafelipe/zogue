class AddExecutionIndexToHumanInteractions < ActiveRecord::Migration
  def change
    add_column :human_interactions, :execution_index, :string
  end
end
