class AddSubcategoryIdToNews < ActiveRecord::Migration
  def change
    add_column :news, :subcategory_id, :string
  end
end
