
class CreateClipsNewsJoin < ActiveRecord::Migration
  def up
    create_table :clips_news, :id => false do |t|
    	t.integer "clip_id"
    	t.integer "news_id"
    end
    add_index :clips_news, ["clip_id", "news_id"]
  end

  def down
  	drop_table :clips_news
  end

end



