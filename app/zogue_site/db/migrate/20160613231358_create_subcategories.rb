class CreateSubcategories < ActiveRecord::Migration
  def change
    create_table :subcategories do |t|
      t.string :Name
      t.text :Description
      t.integer :category_id

      t.timestamps null: false
    end
  end
end
