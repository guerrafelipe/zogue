class AddExecutionIndexToNews < ActiveRecord::Migration
  def change
    add_column :news, :execution_index, :string
  end
end
