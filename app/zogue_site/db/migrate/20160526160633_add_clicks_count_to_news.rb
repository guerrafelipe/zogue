class AddClicksCountToNews < ActiveRecord::Migration
  def change
    add_column :news, :clicks_count, :integer
  end
end
