class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :Title
      t.text :Text
      t.string :Author
      t.string :Url

      t.timestamps null: false
    end
  end
end
