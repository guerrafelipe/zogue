class AddNewsIdToBranches < ActiveRecord::Migration
  def change
    add_column :branches, :news_id, :integer
  end
end
