class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.string :Name
      t.string :Url
      t.string :Country
      t.string :State
      t.string :Model
      t.integer :category_id
      t.datetime :last_scrapped
      t.integer :score

      t.timestamps null: false
    end
  end
end
