# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160703234823) do

  create_table "bootsy_image_galleries", force: :cascade do |t|
    t.integer  "bootsy_resource_id",   limit: 4
    t.string   "bootsy_resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bootsy_images", force: :cascade do |t|
    t.string   "image_file",       limit: 255
    t.integer  "image_gallery_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "branches", force: :cascade do |t|
    t.string   "Title",                   limit: 255
    t.text     "Text",                    limit: 65535
    t.integer  "new_id",                  limit: 4
    t.string   "reaction",                limit: 255
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.string   "author",                  limit: 255
    t.integer  "news_id",                 limit: 4
    t.integer  "cached_votes_total",      limit: 4,     default: 0
    t.integer  "cached_votes_score",      limit: 4,     default: 0
    t.integer  "cached_votes_up",         limit: 4,     default: 0
    t.integer  "cached_votes_down",       limit: 4,     default: 0
    t.integer  "cached_weighted_score",   limit: 4,     default: 0
    t.integer  "cached_weighted_total",   limit: 4,     default: 0
    t.float    "cached_weighted_average", limit: 24,    default: 0.0
  end

  add_index "branches", ["cached_votes_down"], name: "index_branches_on_cached_votes_down", using: :btree
  add_index "branches", ["cached_votes_score"], name: "index_branches_on_cached_votes_score", using: :btree
  add_index "branches", ["cached_votes_total"], name: "index_branches_on_cached_votes_total", using: :btree
  add_index "branches", ["cached_votes_up"], name: "index_branches_on_cached_votes_up", using: :btree
  add_index "branches", ["cached_weighted_average"], name: "index_branches_on_cached_weighted_average", using: :btree
  add_index "branches", ["cached_weighted_score"], name: "index_branches_on_cached_weighted_score", using: :btree
  add_index "branches", ["cached_weighted_total"], name: "index_branches_on_cached_weighted_total", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "Name",        limit: 255
    t.text     "Description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "categories_users", id: false, force: :cascade do |t|
    t.integer "category_id", limit: 4
    t.integer "user_id",     limit: 4
  end

  add_index "categories_users", ["category_id", "user_id"], name: "index_categories_users_on_category_id_and_user_id", using: :btree

  create_table "clips", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.integer  "user_id",     limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "clips_news", id: false, force: :cascade do |t|
    t.integer "clip_id", limit: 4
    t.integer "news_id", limit: 4
  end

  add_index "clips_news", ["clip_id", "news_id"], name: "index_clips_news_on_clip_id_and_news_id", using: :btree

  create_table "human_interactions", force: :cascade do |t|
    t.string   "Title",              limit: 255
    t.string   "suggested_category", limit: 255
    t.integer  "user_id",            limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "new_id",             limit: 4
    t.string   "execution_index",    limit: 255
    t.boolean  "add_entities"
    t.boolean  "review"
  end

  create_table "news", force: :cascade do |t|
    t.string   "Title",                   limit: 255
    t.text     "Text",                    limit: 65535
    t.string   "Author",                  limit: 255
    t.string   "Url",                     limit: 255
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.string   "image_file_name",         limit: 255
    t.string   "image_content_type",      limit: 255
    t.integer  "image_file_size",         limit: 4
    t.datetime "image_updated_at"
    t.integer  "category_id",             limit: 4
    t.date     "news_edition"
    t.integer  "cached_votes_total",      limit: 4,     default: 0
    t.integer  "cached_votes_score",      limit: 4,     default: 0
    t.integer  "cached_votes_up",         limit: 4,     default: 0
    t.integer  "cached_votes_down",       limit: 4,     default: 0
    t.integer  "cached_weighted_score",   limit: 4,     default: 0
    t.integer  "cached_weighted_total",   limit: 4,     default: 0
    t.float    "cached_weighted_average", limit: 24,    default: 0.0
    t.string   "execution_index",         limit: 255
    t.integer  "subcategory_id",          limit: 4
    t.boolean  "already_known"
    t.string   "people_list",             limit: 255
    t.string   "organizations_list",      limit: 255
  end

  add_index "news", ["cached_votes_down"], name: "index_news_on_cached_votes_down", using: :btree
  add_index "news", ["cached_votes_score"], name: "index_news_on_cached_votes_score", using: :btree
  add_index "news", ["cached_votes_total"], name: "index_news_on_cached_votes_total", using: :btree
  add_index "news", ["cached_votes_up"], name: "index_news_on_cached_votes_up", using: :btree
  add_index "news", ["cached_weighted_average"], name: "index_news_on_cached_weighted_average", using: :btree
  add_index "news", ["cached_weighted_score"], name: "index_news_on_cached_weighted_score", using: :btree
  add_index "news", ["cached_weighted_total"], name: "index_news_on_cached_weighted_total", using: :btree

  create_table "pictures", force: :cascade do |t|
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.integer  "category_id",        limit: 4
    t.integer  "subcategory_id",     limit: 4
  end

  create_table "sites", force: :cascade do |t|
    t.string   "Name",          limit: 255
    t.string   "Url",           limit: 255
    t.string   "Country",       limit: 255
    t.string   "State",         limit: 255
    t.string   "Model",         limit: 255
    t.integer  "category_id",   limit: 4
    t.datetime "last_scrapped"
    t.integer  "score",         limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "statistics", force: :cascade do |t|
    t.integer  "listed_sites",           limit: 4
    t.float    "genesis",                limit: 24
    t.float    "runner",                 limit: 24
    t.float    "primary_classification", limit: 24
    t.integer  "news_number",            limit: 4
    t.integer  "rightansw_number",       limit: 4
    t.float    "global",                 limit: 24
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.text     "comment",                limit: 65535
  end

  create_table "subcategories", force: :cascade do |t|
    t.string   "Name",        limit: 255
    t.text     "Description", limit: 65535
    t.integer  "category_id", limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255,   default: "",    null: false
    t.string   "encrypted_password",     limit: 255,   default: "",    null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.string   "role",                   limit: 255
    t.string   "avatar_file_name",       limit: 255
    t.string   "avatar_content_type",    limit: 255
    t.integer  "avatar_file_size",       limit: 4
    t.datetime "avatar_updated_at"
    t.float    "latitude",               limit: 24
    t.float    "longitude",              limit: 24
    t.string   "name",                   limit: 255
    t.boolean  "is_admin"
    t.text     "about",                  limit: 65535
    t.string   "username",               limit: 255
    t.boolean  "email_confirmed",                      default: false
    t.string   "confirm_token",          limit: 255
    t.string   "provider",               limit: 255
    t.string   "uid",                    limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "votes", force: :cascade do |t|
    t.integer  "votable_id",   limit: 4
    t.string   "votable_type", limit: 255
    t.integer  "voter_id",     limit: 4
    t.string   "voter_type",   limit: 255
    t.boolean  "vote_flag"
    t.string   "vote_scope",   limit: 255
    t.integer  "vote_weight",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope", using: :btree
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope", using: :btree

end
